#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/7/24 08:44

from flask import Blueprint, request, current_app
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from app.utils.util import route
from app.utils.auth import auth
from app.utils.oplogger import OperateLogger
from app.api.tree import FeatureClassTree
import json

bp = Blueprint("feature", __name__, url_prefix='/zrdcg/v1/feature/')
dblog = OperateLogger()

# 设置公用方法，更新目录数量
sql_fea_cnt = 'WITH RECURSIVE cte AS ( SELECT Id, FeatureClassPId FROM tb_feature_class WHERE Id=%s ' \
              'UNION ALL SELECT t.Id, t.FeatureClassPId FROM tb_feature_class t ' \
              'JOIN cte c ON c.FeatureClassPId = t.Id ) UPDATE tb_feature_class x, cte y, ' \
              '(SELECT FeatureClassId, COUNT(1) AS FeatureCount FROM tb_feature_info ' \
              'WHERE FeatureClassId=%s AND Deleted=0) z SET x.FeatureCount=z.FeatureCount WHERE x.Id=y.Id ' \
              'AND x.Id=y.FeatureClassPId AND x.Deleted=0;'

sql_level = 'WITH RECURSIVE cte AS ( SELECT Id, FeatureClassPId FROM tb_feature_class WHERE Id = %s ' \
            'UNION ALL SELECT t.Id, t.FeatureClassPId FROM tb_feature_class t ' \
            'JOIN cte c ON c.FeatureClassPId = t.Id ) ' \
            'SELECT COUNT(1) AS ClassCount FROM cte;'


@route(bp, '/AddFeatureClass', methods=["POST"])
@auth.login_required
def addfc():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        class_name = req_data['FeatureClassName']
        class_path = req_data['FeatureClassPath']
        class_pid = req_data['FeatureClassPId']
        class_index = req_data['ClassIndex']
        is_internal = req_data['IsInternal']
        if not class_name:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证同一级下分类名称是否存在
        sql_name_exists = 'SELECT Id FROM tb_feature_class ' \
                          'WHERE FeatureClassName = %s AND FeatureClassPId = %s AND Deleted = 0;'

        cur_task.execute(sql_name_exists, [class_name, class_pid])
        res_class_name = cur_task.fetchone()
        if res_class_name:
            res.update(code=ResponseCode.ClassNameExists, msg=ResponseMessage.ClassNameExists)
            return res.data

        # 验证上级分类目录是否存在
        if class_pid:
            sql_pid_exists = 'SELECT Id FROM tb_feature_class WHERE Id = %s AND Deleted = 0;'

            cur_task.execute(sql_pid_exists, [class_pid])
            res_class_pid = cur_task.fetchone()
            if not res_class_pid:
                res.update(code=ResponseCode.ClassPidNotExists, msg=ResponseMessage.ClassPidNotExists)
                return res.data
        else:
            class_pid = None

        # 验证分类层级是否大于4
        if class_pid:
            cur_task.execute(sql_level, [class_pid])
            res_class_count = cur_task.fetchall()
            if res_class_count:
                res_class_count = res_class_count[0].get('ClassCount')
                if res_class_count > 3:
                    res.update(code=ResponseCode.ClassLevelMoreThan4, msg=ResponseMessage.ClassLevelMoreThan4)
                    return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        # nextval
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['FC'])
        res_cur = cur_task.fetchone()
        class_id = res_cur.get('Id')

        sql_feature_class_ins = 'INSERT INTO tb_feature_class (Id, FeatureClassName, FeatureClassPath, ' \
                                'FeatureClassPId, ClassIndex, IsInternal, CreatedById, UpdatedbyId) ' \
                                'VALUES (%s, %s, %s, %s, %s, %s, %s, %s);'
        params_class = [class_id, class_name, class_path, class_pid, class_index,
                        is_internal, current_user_id, current_user_id]

        cur_task.execute(sql_feature_class_ins, params_class)
        succ_id = dict(FeatureClassId=class_id)
        res.update(data=succ_id)

        # 更新数量
        cur_task.execute(sql_fea_cnt, [class_id, class_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelFeatureClass', methods=["POST"])
@auth.login_required
def delfc():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        class_id = req_data['FeatureClassId']
        if not class_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证分类目录是否存在
        sql_id_exists = 'SELECT Id FROM tb_feature_class WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_id_exists, [class_id])
        res_class_id = cur_task.fetchall()
        if not res_class_id:
            res.update(code=ResponseCode.ClassIdNotExists, msg=ResponseMessage.ClassIdNotExists)
            return res.data

        # 验证是否有下级分类
        sql_cid_exists = 'SELECT Id FROM tb_feature_class WHERE FeatureClassPId = %s AND Deleted = 0;'

        cur_task.execute(sql_cid_exists, [class_id])
        res_class_cid = cur_task.fetchall()
        if res_class_cid:
            res.update(code=ResponseCode.ClassChildExists, msg=ResponseMessage.ClassChildExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_class_del = 'UPDATE tb_feature_class ' \
                        'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                        'WHERE Id = %s;'

        cur_task.execute(sql_class_del, [current_user_id, class_id])

        # 更新数量
        cur_task.execute(sql_fea_cnt, [class_id, class_id])
        con_task.commit()

        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateFeatureClass', methods=["POST"])
@auth.login_required
def updatefc():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        class_id = req_data['FeatureClassId']
        class_name = req_data['FeatureClassName']
        class_path = req_data['FeatureClassPath']
        class_pid = req_data['FeatureClassPid']
        class_index = req_data['ClassIndex']
        is_internal = req_data['IsInternal']
        if not class_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证同一级下分类名称是否存在(不包含本条)
        sql_name_exists = 'SELECT Id FROM tb_feature_class WHERE Id !=%s '
        sql_params = []
        if class_id is not None:
            sql_name_exists += 'AND FeatureClassName = %s '
            sql_params.append(class_name)
        if class_pid is not None:
            sql_name_exists += 'AND FeatureClassPId = %s '
            sql_params.append(class_pid)
        sql_name_exists += 'AND Deleted = 0;'
        cur_task.execute(sql_name_exists, sql_params)
        res_class_name = cur_task.fetchone()
        if res_class_name:
            res.update(code=ResponseCode.ClassNameExists, msg=ResponseMessage.ClassNameExists)
            return res.data

        # 验证分类目录是否存在
        sql_pid_exists = 'SELECT Id, FeatureClassName, FeatureClassPath, FeatureClassPId, ClassIndex, IsInternal ' \
                         'FROM tb_feature_class WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_pid_exists, [class_id])
        res_class = cur_task.fetchone()
        if not res_class:
            res.update(code=ResponseCode.ClassIdNotExists, msg=ResponseMessage.ClassIdNotExists)
            return res.data
        else:
            res_class_name = res_class.get('FeatureClassName')
            res_class_path = res_class.get('FeatureClassPath')
            res_class_pid = res_class.get('FeatureClassPId')
            res_class_index = res_class.get('ClassIndex')
            res_is_internal = res_class.get('IsInternal')

        # 验证分类层级是否大于4
        if class_pid:
            cur_task.execute(sql_level, [class_pid])
            res_class_count = cur_task.fetchall()
            if res_class_count:
                res_class_count = res_class_count[0].get('ClassCount')
                if res_class_count > 3:
                    res.update(code=ResponseCode.ClassLevelMoreThan4, msg=ResponseMessage.ClassLevelMoreThan4)
                    return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_class_update = 'UPDATE tb_feature_class SET UpdatedbyId = %s '
        if res_class_name != class_name:
            sql_class_update += ',FeatureClassName = %s '
            params_update.append(class_name)
        if res_class_path != class_path:
            sql_class_update += ',FeatureClassPath = %s '
            params_update.append(class_path)
        if res_class_pid != class_pid:
            sql_class_update += ',FeatureClassPId = %s '
            params_update.append(class_pid)
        if res_class_index != class_index:
            sql_class_update += ',ClassIndex = %s '
            params_update.append(class_index)
        if res_is_internal != is_internal:
            sql_class_update += ',IsInternal = %s '
            params_update.append(is_internal)
        sql_class_update += 'WHERE Id = %s;'
        params_update.append(class_id)
        cur_task.execute(sql_class_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetFeatureClassList', methods=["POST"])
@auth.login_required
def getfc():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        class_id = req_data['Id']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text = 'WITH RECURSIVE cte AS ( SELECT Id, FeatureClassName, FeatureClassPath, ' \
               'FeatureClassPId, ClassIndex, IsInternal, FeatureCount, CreatedById, CreatedAt, ' \
               'UpdatedbyId, UpdatedAt FROM tb_feature_class WHERE Deleted = 0 '
    params = []
    if isinstance(class_id, str):
        sql_text += "AND Id = %s UNION ALL SELECT t.Id, t.FeatureClassName, t.FeatureClassPath, t.FeatureClassPId, " \
                    "t.ClassIndex, t.IsInternal, t.FeatureCount, t.CreatedById, t.CreatedAt, t.UpdatedbyId, " \
                    "t.UpdatedAt FROM tb_feature_class t JOIN cte c ON t.FeatureClassPId = c.Id WHERE t.Deleted = 0) " \
                    "SELECT Id, FeatureClassName, FeatureClassPath, IF(Id = %s, NULL, FeatureClassPId)  " \
                    "AS FeatureClassPId, ClassIndex, IsInternal, FeatureCount, CreatedById, CreatedAt, " \
                    "UpdatedbyId, UpdatedAt FROM cte ORDER BY Id;"
        params.append(class_id)
        params.append(class_id)
    elif class_id is None:
        sql_text += "AND FeatureClassPId IS NULL UNION ALL SELECT t.Id, t.FeatureClassName, t.FeatureClassPath, " \
                    "t.FeatureClassPId, " \
                    "t.ClassIndex, t.IsInternal, t.FeatureCount, t.CreatedById, t.CreatedAt, t.UpdatedbyId, " \
                    "t.UpdatedAt FROM tb_feature_class t JOIN cte c ON t.FeatureClassPId = c.Id WHERE t.Deleted = 0) " \
                    "SELECT Id, FeatureClassName, FeatureClassPath, FeatureClassPId, ClassIndex, IsInternal, " \
                    "FeatureCount, CreatedById, CreatedAt, UpdatedbyId, UpdatedAt FROM cte ORDER BY Id;"
    else:
        result = dict(error='参数Id传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        new_tree = FeatureClassTree(data=result)
        result_tree = new_tree.build_tree()
        res.update(data=result_tree)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/AddFeatureInfo', methods=["POST"])
@auth.login_required
def addfi():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        class_id = req_data['FeatureClassId']
        feature_name = req_data['FeatureName']
        feature_status = req_data['FeatureStatus']
        feature_rule = json.dumps(req_data['FeatureRule'])
        is_internal = req_data['IsInternal']
        if not feature_name or not class_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征名称是否存在
        sql_name_exists = 'SELECT Id FROM tb_feature_info WHERE FeatureName = %s AND Deleted = 0;'

        cur_task.execute(sql_name_exists, [feature_name])
        res_feature_name = cur_task.fetchone()
        if res_feature_name:
            res.update(code=ResponseCode.FeatureNameExists, msg=ResponseMessage.FeatureNameExists)
            return res.data

        # 验证特征分类目录是否存在
        sql_class_exists = 'SELECT Id FROM tb_feature_class WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_class_exists, [class_id])
        res_class_pid = cur_task.fetchone()
        if not res_class_pid:
            res.update(code=ResponseCode.ClassIdNotExists, msg=ResponseMessage.ClassIdNotExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        # nextval
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['FI'])
        res_cur = cur_task.fetchone()
        feature_id = res_cur.get('Id')

        sql_feature_insert = 'INSERT INTO tb_feature_info (Id, FeatureClassId, FeatureName, FeatureStatus, ' \
                             'FeatureRule, IsInternal, CreatedById, UpdatedbyId) ' \
                             'VALUES (%s, %s, %s, %s, %s, %s, %s, %s);'
        params_feature = [feature_id, class_id, feature_name, feature_status, feature_rule,
                          is_internal, current_user_id, current_user_id]

        cur_task.execute(sql_feature_insert, params_feature)
        # 更新数量
        cur_task.execute(sql_fea_cnt, [class_id, class_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelFeatureInfo', methods=["POST"])
@auth.login_required
def delfi():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        feature_id = req_data['FeatureId']
        if not feature_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征信息是否存在
        sql_feature_exists = 'SELECT FeatureClassId FROM tb_feature_info WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_feature_exists, [feature_id])
        res_class_id = cur_task.fetchall()
        if not res_class_id:
            res.update(code=ResponseCode.FeatureIdNotExists, msg=ResponseMessage.FeatureIdNotExists)
            return res.data
        else:
            class_id = res_class_id.get('FeatureClassId')

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_feature_del = 'UPDATE tb_feature_info ' \
                          'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                          'WHERE Id = %s;'

        cur_task.execute(sql_feature_del, [current_user_id, feature_id])
        # 更新数量
        cur_task.execute(sql_fea_cnt, [class_id, class_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateFeatureInfo', methods=["POST"])
@auth.login_required
def updatefi():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        feature_id = req_data['FeatureId']
        # class_id = req_data['FeatureClassId']
        feature_name = req_data['FeatureName']
        feature_status = req_data['FeatureStatus']
        feature_rule = json.dumps(req_data['FeatureRule'])
        is_internal = req_data['IsInternal']
        # Id必填
        if not feature_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征名称是否存在
        if feature_name:
            sql_name_exists = 'SELECT Id FROM tb_feature_info WHERE FeatureName = %s ' \
                              'AND Id !=%s AND Deleted = 0;'

            cur_task.execute(sql_name_exists, [feature_name, feature_id])
            res_feature_name = cur_task.fetchone()
            if res_feature_name:
                res.update(code=ResponseCode.FeatureNameExists, msg=ResponseMessage.FeatureNameExists)
                return res.data

        # 验证特征信息是否存在
        sql_feature_exists = 'SELECT Id, FeatureClassId, FeatureName, FeatureStatus, FeatureRule, IsInternal ' \
                             'FROM tb_feature_info WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_feature_exists, [feature_id])
        res_class = cur_task.fetchone()
        if not res_class:
            res.update(code=ResponseCode.FeatureIdNotExists, msg=ResponseMessage.FeatureIdNotExists)
            return res.data
        else:
            # res_class_id = res_class.get('FeatureClassId')
            res_feature_name = res_class.get('FeatureName')
            res_feature_status = res_class.get('FeatureStatus')
            res_feature_rule = res_class.get('FeatureRule')
            res_is_internal = res_class.get('IsInternal')

        # 验证分类目录是否存在
        # sql_pid_exists = 'SELECT Id \
        # FROM tb_feature_class \
        # WHERE Id = %s \
        # AND Deleted = 0;'

        # cur_task.execute(sql_pid_exists, [class_id])
        # res_class = cur_task.fetchone()
        # if not res_class:
        #     res.update(code=ResponseCode.ClassIdNotExists, msg=ResponseMessage.ClassIdNotExists)
        #     return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_feature_update = 'UPDATE tb_feature_info SET UpdatedbyId = %s '
        # if res_class_id != class_id:
        #     sql_feature_update += ',FeatureClassId = %s '
        #     params_update.append(class_id)
        if res_feature_name != feature_name:
            sql_feature_update += ',FeatureName = %s '
            params_update.append(feature_name)
        if res_feature_status != feature_status:
            sql_feature_update += ',FeatureStatus = %s '
            params_update.append(feature_status)
        if res_feature_rule != feature_rule:
            sql_feature_update += ',FeatureRule = %s '
            params_update.append(feature_rule)
        if res_is_internal != is_internal:
            sql_feature_update += ',IsInternal = %s '
            params_update.append(is_internal)
        sql_feature_update += 'WHERE Id = %s;'
        params_update.append(feature_id)
        cur_task.execute(sql_feature_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetFeatureList', methods=["POST"])
@auth.login_required
def getfi():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        class_id = req_data['FeatureClassId']
        feature_name = req_data['FeatureName']
        feature_status = req_data['FeatureStatus']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = 'SELECT * FROM tb_feature_info WHERE 1=1 '

    params = []
    if isinstance(class_id, str):
        sql_text += "AND FeatureClassId = %s "
        params.append(class_id)
    elif class_id is None:
        pass
    else:
        result = dict(error='参数FeatureClassId传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(feature_name, str):
        sql_text += "AND FeatureName LIKE %s "
        params.append('%' + feature_name + '%')
    elif feature_name is None:
        pass
    else:
        result = dict(error='参数FeatureName传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(feature_status, int):
        sql_text += "AND FeatureStatus = %s "
        params.append(feature_status)
    elif feature_status is None:
        pass
    else:
        result = dict(error='参数FeatureStatus传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text += "AND Deleted = 0 ORDER BY Id "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(int(page_size))
    elif page_number is None:
        pass
    else:
        result = dict(error='参数page传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        for item in result:
            item["FeatureRule"] = json.loads(item["FeatureRule"])
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/AddFeatureDict', methods=["POST"])
@auth.login_required
def addfd():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dict_name = req_data['FeatureDictName']
        dict_desc = req_data['FeatureDictDesc']
        # 名称必填
        if not dict_name:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征字典名称是否存在
        sql_name_exists = 'SELECT Id FROM tb_feature_dict WHERE FeatureDictName = %s AND Deleted = 0;'

        cur_task.execute(sql_name_exists, [dict_name])
        res_dict_name = cur_task.fetchone()
        if res_dict_name:
            res.update(code=ResponseCode.DictNameExists, msg=ResponseMessage.DictNameExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        # nextval
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['FD'])
        res_cur = cur_task.fetchone()
        dict_id = res_cur.get('Id')

        sql_dict_insert = 'INSERT INTO tb_feature_dict (Id, FeatureDictName, FeatureDictDesc, ' \
                          'CreatedById, UpdatedbyId) VALUES (%s, %s, %s, %s, %s);'
        params_dict = [dict_id, dict_name, dict_desc, current_user_id, current_user_id]

        cur_task.execute(sql_dict_insert, params_dict)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelFeatureDict', methods=["POST"])
@auth.login_required
def delfd():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dict_id = req_data['FeatureDictId']
        # Id必填
        if not dict_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征字典是否存在
        sql_dict_exists = 'SELECT Id FROM tb_feature_dict WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_dict_exists, [dict_id])
        res_dict_id = cur_task.fetchone()
        if not res_dict_id:
            res.update(code=ResponseCode.DictIdNotExists, msg=ResponseMessage.DictIdNotExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_dict_del = 'UPDATE tb_feature_dict ' \
                       'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                       'WHERE Id = %s;'

        cur_task.execute(sql_dict_del, [current_user_id, dict_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateFeatureDict', methods=["POST"])
@auth.login_required
def updatefd():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dict_id = req_data['FeatureDictId']
        dict_name = req_data['FeatureDictName']
        dict_desc = req_data['FeatureDictDesc']
        # Id必填
        if not dict_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征字典名称是否存在
        if dict_name:
            sql_name_exists = 'SELECT Id FROM tb_feature_dict WHERE FeatureDictName = %s ' \
                              'AND Id !=%s AND Deleted = 0;'

            cur_task.execute(sql_name_exists, [dict_id, dict_name])
            res_dict_name = cur_task.fetchone()
            if res_dict_name:
                res.update(code=ResponseCode.DictNameExists, msg=ResponseMessage.DictNameExists)
                return res.data

        # 验证特征字典是否存在
        sql_dict_exists = 'SELECT Id FROM tb_feature_dict WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_dict_exists, [dict_id])
        res_class = cur_task.fetchone()
        if not res_class:
            res.update(code=ResponseCode.DictIdNotExists, msg=ResponseMessage.DictIdNotExists)
            return res.data
        else:
            res_dict_name = res_class.get('FeatureDictName')
            res_dict_desc = res_class.get('FeatureDictDesc')

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_dict_update = 'UPDATE tb_feature_dict SET UpdatedbyId = %s '
        if res_dict_name != dict_name:
            sql_dict_update += ',FeatureDictName = %s '
            params_update.append(dict_name)
        if res_dict_desc != dict_desc:
            sql_dict_update += ',FeatureDictDesc = %s '
            params_update.append(dict_desc)
        sql_dict_update += 'WHERE Id = %s;'
        params_update.append(dict_id)
        cur_task.execute(sql_dict_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetFeatureDict', methods=["POST"])
@auth.login_required
def getfd():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dict_id = req_data['FeatureDictId']
        dict_name = req_data['FeatureDictName']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = 'SELECT * FROM tb_feature_dict WHERE 1=1 '

    params = []
    if isinstance(dict_id, str):
        sql_text += "AND Id = %s "
        params.append(dict_id)
    elif dict_id is None:
        pass
    else:
        result = dict(error='参数FeatureDictId传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(dict_name, str):
        sql_text += "AND FeatureDictName LIKE %s "
        params.append('%' + dict_name + '%')
    elif dict_name is None:
        pass
    else:
        result = dict(error='参数FeatureDictName传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text += "AND Deleted = 0 ORDER BY Id "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(int(page_size))
    elif page_number is None:
        pass
    else:
        result = dict(error='参数page传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/AddDictSample', methods=["POST"])
@auth.login_required
def addds():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dict_id = req_data['FeatureDictId']
        sample_name = req_data['DictSampleName']
        # FeatureDictId必填
        if not dict_id or not sample_name:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征字典名称是否存在
        sql_sample_exists = 'SELECT Id FROM tb_feature_dict_sample ' \
                            'WHERE FeatureDictId = %s ' \
                            'AND DictSampleName = %s AND Deleted = 0;'

        cur_task.execute(sql_sample_exists, [dict_id, sample_name])
        res_dict_name = cur_task.fetchone()
        if res_dict_name:
            res.update(code=ResponseCode.SampleNameExists, msg=ResponseMessage.SampleNameExists)
            return res.data

        # 验证特征字典名称是否存在
        sql_dict_exists = 'SELECT Id FROM tb_feature_dict WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_dict_exists, [dict_id])
        res_dict_name = cur_task.fetchone()
        if res_dict_name:
            res.update(code=ResponseCode.DictIdNotExists, msg=ResponseMessage.DictIdNotExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_sample_insert = 'INSERT INTO tb_feature_dict_sample (FeatureDictId, DictSampleName, ' \
                            'CreatedById, UpdatedbyId) VALUES (%s, %s, %s, %s);'
        params_dict = [dict_id, sample_name, current_user_id, current_user_id]

        cur_task.execute(sql_sample_insert, params_dict)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelDictSample', methods=["POST"])
@auth.login_required
def delds():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        sample_id = req_data['DictSampleId']
        # DictSampleId 必填
        if not sample_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征字典是否存在
        sql_sample_exists = 'SELECT Id FROM tb_feature_dict_sample WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_sample_exists, [sample_id])
        res_dict_id = cur_task.fetchone()
        if not res_dict_id:
            res.update(code=ResponseCode.SampleIdNotExists, msg=ResponseMessage.SampleIdNotExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_sample_del = 'UPDATE tb_feature_dict_sample ' \
                         'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                         'WHERE Id = %s;'

        cur_task.execute(sql_sample_del, [current_user_id, sample_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateDictSample', methods=["POST"])
@auth.login_required
def updateds():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        sample_id = req_data['DictSampleId']
        dict_id = req_data['FeatureDictId']
        sample_name = req_data['DictSampleName']
        # DictSampleId 必填
        if not sample_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证特征字典名称是否存在
        if sample_name:
            sql_sample_exists = 'SELECT Id FROM tb_feature_dict_sample ' \
                                'WHERE FeatureDictId = %s ' \
                                'AND DictSampleName = %s ' \
                                'AND Id != %s AND Deleted = 0;'

            cur_task.execute(sql_sample_exists, [dict_id, sample_name, sample_id])
            res_dict_name = cur_task.fetchone()
            if res_dict_name:
                res.update(code=ResponseCode.SampleNameExists, msg=ResponseMessage.SampleNameExists)
                return res.data

        # 验证特征字典样本值是否存在
        sql_sample_exists = 'SELECT Id, FeatureDictId, DictSampleName ' \
                            'FROM tb_feature_dict_sample ' \
                            'WHERE Id = %s ' \
                            'AND Deleted = 0;'

        cur_task.execute(sql_sample_exists, [sample_id])
        res_sample = cur_task.fetchone()
        if not res_sample:
            res.update(code=ResponseCode.SampleIdNotExists, msg=ResponseMessage.SampleIdNotExists)
            return res.data
        else:
            res_dict_id = res_sample.get('FeatureDictId')
            res_sample_name = res_sample.get('DictSampleName')

        # 验证特征字典是否存在
        if dict_id:
            sql_dic_exists = 'SELECT Id FROM tb_feature_dict WHERE Id = %s AND Deleted = 0;'

            cur_task.execute(sql_dic_exists, [dict_id])
            res_dict_exit = cur_task.fetchone()
            if not res_dict_exit:
                res.update(code=ResponseCode.DictIdNotExists, msg=ResponseMessage.DictIdNotExists)
                return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_sample_update = 'UPDATE tb_feature_dict_sample SET UpdatedbyId = %s '
        if res_dict_id != dict_id:
            sql_sample_update += ',FeatureDictId = %s '
            params_update.append(dict_id)
        if res_sample_name != sample_name:
            sql_sample_update += ',DictSampleName = %s '
            params_update.append(sample_name)
        sql_sample_update += 'WHERE Id = %s;'
        params_update.append(sample_id)
        cur_task.execute(sql_sample_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetDictSample', methods=["POST"])
@auth.login_required
def getds():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dict_id = req_data['FeatureDictId']
        sample_name = req_data['DictSampleName']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = 'SELECT * FROM tb_feature_dict_sample WHERE 1=1 '

    params = []
    if isinstance(dict_id, str):
        sql_text += "AND Id = %s "
        params.append(dict_id)
    elif dict_id is None:
        pass
    else:
        result = dict(error='参数FeatureDictId传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(sample_name, str):
        sql_text += "AND DictSampleName LIKE %s "
        params.append('%' + sample_name + '%')
    elif sample_name is None:
        pass
    else:
        result = dict(error='参数DictSampleName传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text += "AND Deleted = 0 ORDER BY Id DESC "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(int(page_size))
    elif page_number is None:
        pass
    else:
        result = dict(error='参数page传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/AddDesensitization', methods=["POST"])
@auth.login_required
def addde():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        de_dn = req_data['DesenName']
        de_ds = req_data['DesenStatus']
        de_da = req_data['DesenAlgorithm']
        de_ap = req_data['AlgorithmParam']
        # de_dn必填
        if not de_dn or not de_dn:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证脱敏名称是否存在
        sql_dn_exists = 'SELECT Id FROM tb_desensitization_info ' \
                        'WHERE DesenName = %s ' \
                        'AND Deleted = 0;'

        cur_task.execute(sql_dn_exists, [de_dn])
        res_dict_name = cur_task.fetchone()
        if res_dict_name:
            res.update(code=ResponseCode.DesenNameExists, msg=ResponseMessage.DesenNameExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        # nextval
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['DI'])
        res_cur = cur_task.fetchone()
        de_id = res_cur.get('Id')

        sql_desen_insert = 'INSERT INTO tb_desensitization_info (Id, DesenName, DesenStatus, ' \
                           'DesenAlgorithm, AlgorithmParam, CreatedById, UpdatedbyId) ' \
                           'VALUES (%s, %s, %s, %s, %s, %s, %s);'
        params_dict = [de_id, de_dn, de_ds, de_da, de_ap, current_user_id, current_user_id]

        cur_task.execute(sql_desen_insert, params_dict)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelDesensitization', methods=["POST"])
@auth.login_required
def delde():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        de_id = req_data['DesenId']
        # de_id必填
        if not de_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证脱敏规则是否存在
        sql_desen_exists = 'SELECT Id FROM tb_desensitization_info WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_desen_exists, [de_id])
        res_dict_id = cur_task.fetchone()
        if not res_dict_id:
            res.update(code=ResponseCode.DesenIdNotExists, msg=ResponseMessage.DesenIdNotExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_desen_del = 'UPDATE tb_desensitization_info ' \
                        'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                        'WHERE Id = %s;'

        cur_task.execute(sql_desen_del, [current_user_id, de_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateDesensitization', methods=["POST"])
@auth.login_required
def updatede():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        de_id = req_data['DesenId']
        de_dn = req_data['DesenName']
        de_ds = req_data['DesenStatus']
        de_da = req_data['DesenAlgorithm']
        de_ap = req_data['AlgorithmParam']
        # de_id 必填
        if not de_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证脱敏名称是否存在
        if de_dn:
            sql_dn_exists = 'SELECT Id FROM tb_desensitization_info ' \
                            'WHERE DesenName = %s ' \
                            'AND Id != %s ' \
                            'AND Deleted = 0;'

            cur_task.execute(sql_dn_exists, [de_dn, de_id])
            res_dict_name = cur_task.fetchone()
            if res_dict_name:
                res.update(code=ResponseCode.DesenNameExists, msg=ResponseMessage.DesenNameExists)
                return res.data

        # 验证需要修改的脱敏规则是否存在
        sql_desen_exists = 'SELECT Id, DesenName, DesenStatus, DesenAlgorithm, AlgorithmParam ' \
                           'FROM tb_desensitization_info ' \
                           'WHERE Id = %s ' \
                           'AND Deleted = 0;'

        cur_task.execute(sql_desen_exists, [de_id])
        res_de = cur_task.fetchone()
        if not res_de:
            res.update(code=ResponseCode.DesenIdNotExists, msg=ResponseMessage.DesenIdNotExists)
            return res.data
        else:
            res_de_dn = res_de.get('DesenName')
            res_de_ds = res_de.get('DesenStatus')
            res_de_da = res_de.get('DesenAlgorithm')
            res_de_ap = res_de.get('AlgorithmParam')

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_desen_update = 'UPDATE tb_desensitization_info SET UpdatedbyId = %s '
        if res_de_dn != de_dn:
            sql_desen_update += ',DesenName = %s '
            params_update.append(de_dn)
        if res_de_ds != de_ds:
            sql_desen_update += ',DesenStatus = %s '
            params_update.append(de_ds)
        if res_de_da != de_da:
            sql_desen_update += ',DesenAlgorithm = %s '
            params_update.append(de_da)
        if res_de_ap != de_ap:
            sql_desen_update += ',AlgorithmParam = %s '
            params_update.append(de_ap)
        sql_desen_update += 'WHERE Id = %s;'
        params_update.append(de_id)
        cur_task.execute(sql_desen_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetDesensitization', methods=["POST"])
@auth.login_required
def getde():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        de_id = req_data['DesenId']
        de_dn = req_data['DesenName']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = 'SELECT * FROM tb_desensitization_info WHERE 1=1 '

    params = []
    if isinstance(de_id, str):
        sql_text += "AND Id = %s "
        params.append(de_id)
    elif de_id is None:
        pass
    else:
        result = dict(error='参数DesenId传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(de_dn, str):
        sql_text += "AND DesenName LIKE %s "
        params.append('%' + de_dn + '%')
    elif de_dn is None:
        pass
    else:
        result = dict(error='参数DesenName传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text += "AND Deleted = 0 ORDER BY Id DESC "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(int(page_size))
    elif page_number is None:
        pass
    else:
        result = dict(error='参数page传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/AddDataLevel', methods=["POST"])
@auth.login_required
def adddl():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dl_is = req_data['IsSensitive']
        dl_lc = req_data['LevelCode']
        dl_ll = req_data['LevelLabel']
        dl_ln = req_data['LevelName']
        dl_ls = req_data['LevelStatus']
        dl_lp = req_data['LevelPrinciple']
        dl_co = req_data['LevelColor']
        dl_lr = req_data['LevelRemark']
        # dl_ln 必填
        if not dl_ln:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证分级是否存在
        sql_dl_exists = 'SELECT Id FROM tb_data_level WHERE LevelName = %s AND Deleted = 0;'

        cur_task.execute(sql_dl_exists, [dl_ln])
        res_dict_name = cur_task.fetchone()
        if res_dict_name:
            res.update(code=ResponseCode.LevelNameExists, msg=ResponseMessage.LevelNameExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        # nextval
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['DL'])
        res_cur = cur_task.fetchone()
        dl_id = res_cur.get('Id')

        sql_dl_insert = 'INSERT INTO tb_data_level (Id, IsSensitive, LevelCode, ' \
                        'LevelLabel, LevelName, LevelStatus, LevelPrinciple, LevelColor, ' \
                        'LevelRemark, CreatedById, UpdatedbyId) ' \
                        'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);'
        params_dict = [dl_id, dl_is, dl_lc, dl_ll, dl_ln, dl_ls, dl_lp, dl_co,
                       dl_lr, current_user_id, current_user_id]

        cur_task.execute(sql_dl_insert, params_dict)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelDataLevel', methods=["POST"])
@auth.login_required
def deldl():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dl_id = req_data['LevelId']
        # dl_id 必填
        if not dl_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证数据分级是否存在
        sql_dl_exists = 'SELECT Id FROM tb_data_level WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_dl_exists, [dl_id])
        res_dict_id = cur_task.fetchone()
        if not res_dict_id:
            res.update(code=ResponseCode.LevelIdNotExists, msg=ResponseMessage.LevelIdNotExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_dl_del = 'UPDATE tb_data_level ' \
                     'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                     'WHERE Id = %s;'

        cur_task.execute(sql_dl_del, [current_user_id, dl_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateDataLevel', methods=["POST"])
@auth.login_required
def updatedl():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dl_id = req_data['LevelId']
        dl_is = req_data['IsSensitive']
        dl_lc = req_data['LevelCode']
        dl_ll = req_data['LevelLabel']
        dl_ln = req_data['LevelName']
        dl_ls = req_data['LevelStatus']
        dl_lp = req_data['LevelPrinciple']
        dl_co = req_data['LevelColor']
        dl_lr = req_data['LevelRemark']
        # dl_id 必填
        if not dl_id:
            res.update(code=ResponseCode.LevelIdNotExists, msg=ResponseMessage.LevelIdNotExists)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证分级是否存在
        if dl_ln:
            sql_dl_exists = 'SELECT Id FROM tb_data_level ' \
                            'WHERE LevelName = %s ' \
                            'AND Id !=%s ' \
                            'AND Deleted = 0;'

            cur_task.execute(sql_dl_exists, [dl_ln, dl_id])
            res_dict_name = cur_task.fetchone()
            if res_dict_name:
                res.update(code=ResponseCode.LevelNameExists, msg=ResponseMessage.LevelNameExists)
                return res.data

        # 验证需要修改的数据分级是否存在
        sql_dl_exists = 'SELECT Id, IsSensitive, LevelCode, LevelLabel, LevelName, LevelStatus, ' \
                        'LevelPrinciple, LevelColor, LevelRemark FROM tb_data_level ' \
                        'WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_dl_exists, [dl_id])
        res_dl = cur_task.fetchone()
        if not res_dl:
            res.update(code=ResponseCode.LevelIdNotExists, msg=ResponseMessage.LevelIdNotExists)
            return res.data
        else:
            res_dl_is = res_dl.get('IsSensitive')
            res_dl_lc = res_dl.get('LevelCode')
            res_dl_ll = res_dl.get('LevelLabel')
            res_dl_ln = res_dl.get('LevelName')
            res_dl_ls = res_dl.get('LevelStatus')
            res_dl_lp = res_dl.get('LevelPrinciple')
            res_dl_co = res_dl.get('LevelColor')
            res_dl_lr = res_dl.get('LevelRemark')

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_dl_update = 'UPDATE tb_data_level SET UpdatedbyId = %s '
        if res_dl_is != dl_is:
            sql_dl_update += ',IsSensitive = %s '
            params_update.append(dl_is)
        if res_dl_lc != dl_lc:
            sql_dl_update += ',LevelCode = %s '
            params_update.append(dl_lc)
        if res_dl_ll != dl_ll:
            sql_dl_update += ',LevelLabel = %s '
            params_update.append(dl_ll)
        if res_dl_ln != dl_ln:
            sql_dl_update += ',LevelName = %s '
            params_update.append(dl_ln)
        if res_dl_ls != dl_ls:
            sql_dl_update += ',LevelStatus = %s '
            params_update.append(dl_ls)
        if res_dl_lp != dl_lp:
            sql_dl_update += ',LevelPrinciple = %s '
            params_update.append(dl_lp)
        if res_dl_co != dl_co:
            sql_dl_update += ',LevelColor = %s '
            params_update.append(dl_co)
        if res_dl_lr != dl_lr:
            sql_dl_update += ',LevelRemark = %s '
            params_update.append(dl_lr)
        sql_dl_update += 'WHERE Id = %s;'
        params_update.append(dl_id)
        cur_task.execute(sql_dl_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetDataLevel', methods=["POST"])
@auth.login_required
def getdl():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        dl_id = req_data['LevelId']
        dl_ln = req_data['LevelName']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = 'SELECT * FROM tb_data_level WHERE 1=1 '

    params = []
    if isinstance(dl_id, str):
        sql_text += "AND Id = %s "
        params.append(dl_id)
    elif dl_id is None:
        pass
    else:
        result = dict(error='参数LevelId传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(dl_ln, str):
        sql_text += "AND LevelName LIKE %s "
        params.append('%' + dl_ln + '%')
    elif dl_ln is None:
        pass
    else:
        result = dict(error='参数LevelName传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text += "AND Deleted = 0 ORDER BY Id DESC "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(int(page_size))
    elif page_number is None:
        pass
    else:
        result = dict(error='参数page传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data
