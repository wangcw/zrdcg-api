#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/28 08:47

from flask import Blueprint, request
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from app.utils.util import route
from app.utils.auth import auth
from app.utils.database import MysqlPool
from pymysql.cursors import DictCursor
from app.utils.oplogger import OperateLogger

bp = Blueprint("login", __name__, url_prefix='/')
dblog = OperateLogger()


@route(bp, '/zrdcg/v1/AddRole', methods=["POST"])
@auth.login_required
def addrole():
    res = ResMsg()
    obj = request.get_json(force=True)
    user_name = obj.get("user_name")
    pass_word = obj.get("pass_word")

    if HttpAuth.have_user(user_name) != 1:
        HttpAuth.add_user(user_name, pass_word)
        # result = dict(success='用户\'{}\'新增成功！'.format(user_name))
        # res.update(data=result)
        dbloginfo = '用户' + str(user_name) + '新增成功！'
        dblog.operate_log('用户', dbloginfo)
    else:
        result = dict(error='用户名已存在！')
        res.update(code=ResponseCode.UserHaveFond, msg=ResponseMessage.AccountOrPassWordErr, data=result)
    return res.data
