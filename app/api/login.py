#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/21 10:44

from flask import Blueprint, request, current_app
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from app.utils.util import route
from app.utils.auth import auth
from app.utils.oplogger import OperateLogger

bp = Blueprint("login", __name__, url_prefix='/')
dblog = OperateLogger()


@route(bp, '/zrdcg/v1/Login', methods=["POST"])
def login():
    res = ResMsg()
    obj = request.get_json(force=True)
    user_name = obj.get("user_name")
    pass_word = obj.get("pass_word")
    if len(pass_word) >= 8:
        pass_word = pass_word[4:-4]
    if not obj or not user_name:
        res.update(code=ResponseCode.UserOrPasswdIsEmpty, msg=ResponseMessage.UserOrPasswdIsEmpty)
        return res.data

    sql_have_user = 'SELECT 1 AS IsHave  FROM tb_basic_user WHERE UserName = %s AND Deleted=0;'
    sql_fail_times = 'SELECT LoginFailnums FROM tb_basic_user WHERE UserName = %s AND Deleted = 0;'
    sql_login = 'UPDATE tb_basic_user SET LastLogin = NOW() WHERE UserName = %s AND Deleted = 0;'
    sql_fail_update = 'UPDATE tb_basic_user SET LoginFailnums = %s WHERE UserName = %s AND Deleted = 0;'
    sql_lock = 'UPDATE tb_basic_user SET UserStatus = 2, LoginFailnums = %s, LockBeginTime = NOW() ' \
               'WHERE UserName = %s AND Deleted = 0;'

    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_have_user, [user_name])
        cur_res = cur_task.fetchone()
        if not cur_res:
            res.update(code=ResponseCode.UserNotFond, msg=ResponseMessage.UserNotFond)
            return res.data
        print('1', auth.hash_password(pass_word))
        print('2', auth.get_password(user_name))
        if auth.hash_password(pass_word) == auth.get_password(user_name):
            cur_task.execute(sql_login, [user_name])
            return res.data
        else:
            cur_task.execute(sql_fail_times, [user_name])
            cur_res = cur_task.fetchone()
            login_fail_times = cur_res or 0
            if login_fail_times != 0:
                login_fail_times = login_fail_times.get('LoginFailnums')
            if login_fail_times < 3:
                login_fail_times += 1
                if login_fail_times == 3:
                    cur_task.execute(sql_lock, [3, user_name])
                    dbloginfo = '用户' + str(user_name) + '登录错误超过3次，已锁定！'
                    dblog.operate_log('登录', dbloginfo)
                    res.update(code=ResponseCode.UserIsLocked, msg=ResponseMessage.UserIsLocked)
                else:
                    cur_task.execute(sql_fail_update, [login_fail_times, user_name])
                    res.update(code=ResponseCode.UnauthorizedAccess, msg=ResponseMessage.UnauthorizedAccess)
                return res.data
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
        return res.data
    finally:
        cur_task.close()
        con_task.close()
    # return res.data
