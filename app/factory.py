#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023-06-20 10:57:26

import logging
import logging.config
import yaml
import os
from flask import Flask, Blueprint
from app.utils.core import JSONEncoder
from app.api.router import router
from mysql.connector.pooling import MySQLConnectionPool


def create_app(config_name, config_path=None):
    app = Flask(__name__)

    # 添加字符集配置
    app.config['JSON_AS_ASCII'] = False

    # 跨域支持
    def after_request(resp):
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

    app.after_request(after_request)

    # 读取配置文件
    if not config_path:
        pwd = os.getcwd()
        config_path = os.path.join(pwd, 'config/config.yaml')
    if not config_name:
        config_name = 'PRODUCTION'

    # 读取配置文件
    conf = read_yaml(config_name, config_path)
    app.config.update(conf)

    # 更新Celery配置信息 celery_conf = "redis://{}@{}:{}/{}".format(app.config['REDIS_CELERY_PWD'], app.config[
    # 'REDIS_HOST'], app.config['REDIS_PORT'], app.config['REDIS_DB']) celery_app.conf.update({"broker_url":
    # celery_conf, "result_backend": celery_conf})

    # 注册接口
    register_api(app=app, routers=router)

    # 返回json格式转换
    app.json_encoder = JSONEncoder

    # 实例化数据库连接
    cfg_db = app.config['DATABASES']
    cfg_task = cfg_db.get('DB_TASK')
    cfg_asset = cfg_db.get('DB_ASSET')
    cfg_src = cfg_db.get('DB_SRC')
    po_src = MySQLConnectionPool(host=cfg_src['host'],
                                 user=cfg_src['user'],
                                 password=cfg_src['password'],
                                 db=cfg_src['database'],
                                 port=cfg_src['port'],
                                 charset='utf8mb4',
                                 pool_name='po_src',
                                 pool_size=5,
                                 time_zone='+8:00')
    app.config['po_src'] = po_src
    po_asset = MySQLConnectionPool(host=cfg_asset['host'],
                                   user=cfg_asset['user'],
                                   password=cfg_asset['password'],
                                   db=cfg_asset['database'],
                                   port=cfg_asset['port'],
                                   charset='utf8mb4',
                                   pool_name='po_asset',
                                   pool_size=5,
                                   time_zone='+8:00')
    app.config['po_asset'] = po_asset
    po_task = MySQLConnectionPool(host=cfg_task['host'],
                                  user=cfg_task['user'],
                                  password=cfg_task['password'],
                                  db=cfg_task['database'],
                                  port=cfg_task['port'],
                                  charset='utf8mb4',
                                  pool_name='po_task',
                                  pool_size=5,
                                  time_zone='+8:00')
    app.config['po_task'] = po_task

    # 日志文件目录
    if not os.path.exists(app.config['LOGGING_PATH']):
        os.mkdir(app.config['LOGGING_PATH'])

    # 日志设置
    with open(app.config['LOGGING_CONFIG_PATH'], 'r', encoding='utf-8') as f:
        dict_conf = yaml.safe_load(f.read())
    logging.config.dictConfig(dict_conf)

    # 读取msg配置
    with open(app.config['RESPONSE_MESSAGE'], 'r', encoding='utf-8') as f:
        msg = yaml.safe_load(f.read())
        app.config.update(msg)

    return app


def read_yaml(config_name, config_path):
    """
    config_name:需要读取的配置内容
    config_path:配置文件路径
    """
    if config_name and config_path:
        with open(config_path, 'r', encoding='utf-8') as f:
            conf = yaml.safe_load(f.read())
        if config_name in conf.keys():
            return conf[config_name.upper()]
        else:
            raise KeyError('未找到对应的配置信息')
    else:
        raise ValueError('请输入正确的配置名称或配置文件路径')


def register_api(app, routers):
    for router_api in routers:
        if isinstance(router_api, Blueprint):
            app.register_blueprint(router_api)
        else:
            try:
                endpoint = router_api.__name__
                view_func = router_api.as_view(endpoint)
                # 如果没有服务名,默认 类名小写
                if hasattr(router_api, "service_name"):
                    url = '/{}/'.format(router_api.service_name.lower())
                else:
                    url = '/{}/'.format(router_api.__name__.lower())
                if 'GET' in router_api.__methods__:
                    app.add_url_rule(url, defaults={'key': None}, view_func=view_func, methods=['GET', ])
                    app.add_url_rule('{}<string:key>'.format(url), view_func=view_func, methods=['GET', ])
                if 'POST' in router_api.__methods__:
                    app.add_url_rule(url, view_func=view_func, methods=['POST', ])
                if 'PUT' in router_api.__methods__:
                    app.add_url_rule('{}<string:key>'.format(url), view_func=view_func, methods=['PUT', ])
                if 'DELETE' in router_api.__methods__:
                    app.add_url_rule('{}<string:key>'.format(url), view_func=view_func, methods=['DELETE', ])
            except Exception as e:
                raise ValueError(e)
