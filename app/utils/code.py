#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023-06-20 10:53:21

class ResponseCode(object):
    Success = 0  # 成功
    Fail = -1  # 失败
    SqlError = 10001  # SQL执行失败
    NoResourceFound = 40001  # 未找到资源
    InvalidParameter = 40002  # 参数无效
    AccountOrPassWordErr = 40003  # 账户或密码错误
    VerificationCodeError = 40004  # 验证码错误
    PleaseSignIn = 40005  # 请登陆
    WeChatAuthorizationFailure = 40006  # 微信授权失败
    InvalidOrExpired = 40007  # 验证码过期
    MobileNumberError = 40008  # 手机号错误
    FrequentOperation = 40009  # 操作频繁,请稍后再试
    UserNotFond = 40010  # 用户不存在
    UserHaveFond = 40011  # 用户已存在
    UserOrPasswdIsEmpty = 40012  # 用户名或密码为空
    PassWordLessThan6 = 40013  # 密码少于6位
    UserIsLocked = 40014  # 用户已锁定
    UnauthorizedAccess = 50001  # 认证失败
    AssetNameExists = 80001  # 资产名称已存在
    AssetNotExist = 80004  # 资产信息不存在
    InvalidParameterPageNumber = 80002  # 参数PageNumber传输错误
    InvalidParameterInstanceId = 80003  # 参数InstanceId传输错误
    ClassNameExists = 80005  # 特征分类目录已存在
    ClassPidNotExists = 80006  # 特征分类目录Pid不存在
    ClassIdExists = 80007  # 特征分类目录id已存在
    FeatureNameExists = 80008  # "特征名称已存在"
    FeatureIdNotExists = 80009  # "特征信息不存在"
    ClassIdNotExists = 80010  # 特征分类目录id不存在
    ClassChildExists = 80011  # "特征分类目录存在子级"
    DictIdNotExists = 80012  # 特征字典id不存在
    DictNameExists = 80013  # "特征字典名称已存在"
    SampleIdNotExists = 80014  # 特征字典样本不存在
    SampleNameExists = 80015  # "特征字典样本名称已存在"
    DesenNameExists = 80016  # "脱敏规则名称已存在"
    DesenIdNotExists = 80017  # "脱敏规则不存在"
    LevelNameExists = 80018  # "数据分级名称已存在"
    LevelIdNotExists = 80019  # "数据分级Id不存在"
    ClassLevelMoreThan4 = 80020  # "分级目录层级大于4"
    TempNameExists = 80021  # "模版名称已存在"
    TempIdNotExists = 80022  # "模板Id不存在"
    TempClassNameExists = 80023  # "模版分类名称已存在"
    TempClassIdNotExists = 80024  # "模板分类Id不存在"
    TempClassLevelMoreThan4 = 80025  # "模板分级目录层级大于4"
    TempClassChildExists = 80026  # "模板分类目录存在子级"


class ResponseMessage(object):
    Success = "成功"
    Fail = "失败"
    SqlError = "SQL执行失败"
    NoResourceFound = "未找到资源"
    InvalidParameter = "参数无效"
    AccountOrPassWordErr = "账户或密码错误"
    VerificationCodeError = "验证码错误"
    PleaseSignIn = "请登陆"
    UnauthorizedAccess = "认证失败"
    PassWordLessThan6 = "密码少于6位"
    UserIsLocked = "用户已锁定"
    UserHaveFond = "用户已存在"
    UserOrPasswdIsEmpty = "用户名或密码为空"
    AssetNameExists = "资产名称已存在"
    InvalidParameterPageNumber = "参数PageNumber传输错误"
    InvalidParameterInstanceId = "参数InstanceId传输错误"
    AssetNotExist = "资产信息不存在"
    ClassNameExists = "特征分类目录已存在"
    ClassPidNotExists = "特征分类目录Pid不存在"
    ClassIdExists = "特征分类目录id已存在"
    FeatureNameExists = "特征名称已存在"
    FeatureIdNotExists = "特征信息不存在"
    ClassIdNotExists = "特征分类目录id不存在"
    ClassChildExists = "特征分类目录存在子级"
    DictIdNotExists = "特征字典id不存在"
    DictNameExists = "特征字典名称已存在"
    SampleIdNotExists = "特征字典样本不存在"
    SampleNameExists = "特征字典样本名称已存在"
    DesenNameExists = "脱敏规则名称已存在"
    DesenIdNotExists = "脱敏规则不存在"
    LevelNameExists = "数据分级名称已存在"
    LevelIdNotExists = "数据分级Id不存在"
    ClassLevelMoreThan4 = "分级目录层级大于4"
    TempNameExists = "模版名称已存在"
    TempIdNotExists = "模板Id不存在"
    TempClassNameExists = "模版分类名称已存在"
    TempClassIdNotExists = "模板分类Id不存在"
    TempClassLevelMoreThan4 = "模版分级目录层级大于4"
    TempClassChildExists = "模板分类目录存在子级"
