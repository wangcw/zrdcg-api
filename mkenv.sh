#!/usr/bin/sh
# @author by wangcw
# @generate at 2023-06-20 10:58:04

# 虚拟环境创建
# 首先安装python3.8、virtualenv、virtualenvwrapper

mkvirtualenv /Users/wangcw/Documents/github/apiwong-mini/venv -p /opt/homebrew/bin/python3.8 -r /Users/wangcw/Documents/github/apiwong-mini/requirements.txt --clear