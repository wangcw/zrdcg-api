#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023-06-20 10:57:26

from app.factory import create_app

app = create_app(config_name="PRODUCTION")
app.app_context().push()
