#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023-06-20 10:57:03

from app.api.asset import bp as bp_asset
from app.api.login import bp as bp_login
from app.api.user import bp as bp_user
# from app.api.asset_sql import bp as bp_sql
from app.api.feature import bp as bp_feature
from app.api.template import bp as bp_template

router = [
    bp_asset,  # 资产信息接口
    bp_login,  # 用户登录接口
    bp_user,  # 用户信息管理接口
    # bp_sql,  # 测试
    bp_feature,  # 特征信息管理
    bp_template,  # 模板信息管理
]
