#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/27 17:26

from flask import current_app
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg


class OperateLogger(object):

    @staticmethod
    def operate_log(operate_type, operate_info):
        """操作日志写入"""
        res = ResMsg()
        sql_event = 'INSERT INTO tb_operation_log(OperateType, OperateDesc) VALUES(%s, %s)'
        try:
            con_task = current_app.config['po_task'].get_connection()
            cur_task = con_task.cursor(dictionary=True)
            cur_task.execute(sql_event, (operate_type, operate_info))
        except ExceptionGroup as e:
            result = dict(error='操作日志写入错误！')
            res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
        finally:
            cur_task.close()
            con_task.close()
        return res.data

    @staticmethod
    def task_log(operate_type, operate_info):
        """定时任务日志写入"""
        res = ResMsg()
        sql_event = 'INSERT INTO tb_task_log(TaskId, LogInfo) VALUES(%s, %s)'
        try:
            con_task = current_app.config['po_task'].get_connection()
            cur_task = con_task.cursor(dictionary=True)
            cur_task.execute(sql_event, (operate_type, operate_info))
        except ExceptionGroup as e:
            result = dict(error='定时任务日志写入错误！')
            res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
        finally:
            cur_task.close()
            con_task.close()
        return res.data
