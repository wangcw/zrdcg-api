#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/21 11:43

from flask_httpauth import HTTPBasicAuth
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from hashlib import md5
from flask import current_app

auth = HTTPBasicAuth()


@auth.error_handler
def unauthorized():
    res = ResMsg()
    res.update(code=ResponseCode.UnauthorizedAccess, msg=ResponseMessage.UnauthorizedAccess)
    return res.data


@auth.get_password
def get_password(user_name):
    sql_text = 'SELECT PassWord \
    FROM tb_basic_user \
    WHERE UserName = %s \
    AND UserStatus = 1 \
    AND Deleted = 0;'
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, [user_name])
        res_cur = cur_task.fetchone()
        result = res_cur.get('PassWord')
    finally:
        cur_task.close()
        con_task.close()
    return result


@auth.verify_password
def verify_password(user_name, pass_word):
    if len(pass_word) >= 8:
        pass_word = pass_word[4:-4]
    if get_password(user_name) == hash_password(pass_word):
        return True
    return False


@auth.hash_password
def hash_password(pass_word):
    hash_value = md5(pass_word.encode()).hexdigest()
    return hash_value
