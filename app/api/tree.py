#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/8/1 13:47

class FeatureClassTree(object):
    def __init__(self, data):
        self.data = data
        self.root_node = list()
        self.common_node = dict()
        self.tree = list()

    def find_root_node(self, ) -> list:
        for node in self.data:
            if node["FeatureClassPId"] is None:
                self.root_node.append(node)
        return self.root_node

    def find_common_node(self) -> dict:
        for node in self.data:
            father_id = node.get("FeatureClassPId")
            if father_id is not None:
                if father_id not in self.common_node:
                    self.common_node[father_id] = list()
                self.common_node[father_id].append(node)
        return self.common_node

    def build_tree(self, ) -> list:
        self.find_root_node()
        self.find_common_node()
        for root in self.root_node:
            base = dict(Id=root["Id"],
                        FeatureClassName=root["FeatureClassName"],
                        FeatureClassPath=root["FeatureClassPath"],
                        FeatureClassPId=root["FeatureClassPId"],
                        ClassIndex=root["ClassIndex"],
                        IsInternal=root["IsInternal"],
                        FeatureCount=root["FeatureCount"],
                        CreatedById=root["CreatedById"],
                        CreatedAt=root["CreatedAt"],
                        UpdatedbyId=root["UpdatedbyId"],
                        UpdatedAt=root["UpdatedAt"],
                        Child=list())
            self.find_child(base["Id"], base["Child"])
            self.tree.append(base)
        return self.tree

    def find_child(self, father_id: str, child_node: list):
        child_list = self.common_node.get(father_id, [])
        for item in child_list:
            base = dict(Id=item["Id"],
                        FeatureClassName=item["FeatureClassName"],
                        FeatureClassPath=item["FeatureClassPath"],
                        FeatureClassPId=item["FeatureClassPId"],
                        ClassIndex=item["ClassIndex"],
                        IsInternal=item["IsInternal"],
                        FeatureCount=item["FeatureCount"],
                        CreatedById=item["CreatedById"],
                        CreatedAt=item["CreatedAt"],
                        UpdatedbyId=item["UpdatedbyId"],
                        UpdatedAt=item["UpdatedAt"],
                        Child=list())
            self.find_child(item["Id"], base["Child"])
            child_node.append(base)


class TempClassTree(object):
    def __init__(self, data):
        self.data = data
        self.root_node = list()
        self.common_node = dict()
        self.tree = list()

    def find_root_node(self, ) -> list:
        for node in self.data:
            if node["TempClassPId"] is None:
                self.root_node.append(node)
        return self.root_node

    def find_common_node(self) -> dict:
        for node in self.data:
            father_id = node.get("TempClassPId")
            if father_id is not None:
                if father_id not in self.common_node:
                    self.common_node[father_id] = list()
                self.common_node[father_id].append(node)
        return self.common_node

    def build_tree(self, ) -> list:
        self.find_root_node()
        self.find_common_node()
        for root in self.root_node:
            base = dict(Id=root["Id"],
                        TemplateId=root["TemplateId"],
                        TempClassPath=root["TempClassPath"],
                        TempClassName=root["TempClassName"],
                        TempClassPId=root["TempClassPId"],
                        FeatureClassId=root["FeatureClassId"],
                        LevelId=root["LevelId"],
                        ClassIndex=root["ClassIndex"],
                        ClassRemark=root["ClassRemark"],
                        CreatedById=root["CreatedById"],
                        CreatedAt=root["CreatedAt"],
                        UpdatedbyId=root["UpdatedbyId"],
                        UpdatedAt=root["UpdatedAt"],
                        Child=list())
            self.find_child(base["Id"], base["Child"])
            self.tree.append(base)
        return self.tree

    def find_child(self, father_id: str, child_node: list):
        child_list = self.common_node.get(father_id, [])
        for item in child_list:
            base = dict(Id=item["Id"],
                        TemplateId=item["TemplateId"],
                        TempClassPath=item["TempClassPath"],
                        TempClassName=item["TempClassName"],
                        TempClassPId=item["TempClassPId"],
                        FeatureClassId=item["FeatureClassId"],
                        LevelId=item["LevelId"],
                        ClassIndex=item["ClassIndex"],
                        ClassRemark=item["ClassRemark"],
                        CreatedById=item["CreatedById"],
                        CreatedAt=item["CreatedAt"],
                        UpdatedbyId=item["UpdatedbyId"],
                        UpdatedAt=item["UpdatedAt"],
                        Child=list())
            self.find_child(item["Id"], base["Child"])
            child_node.append(base)