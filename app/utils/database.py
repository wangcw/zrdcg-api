#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/16 15:52

import pymysql
from pymysql.cursors import DictCursor
import queue
from flask import current_app
import logging

logger = logging.getLogger(__name__)
pool = queue.Queue()


def create_conn(db_config):
    """创建连接"""
    conn = pymysql.connect(
        host=db_config['host'],
        port=db_config['port'],
        database=db_config['database'],
        user=db_config['user'],
        password=db_config['password'],
        cursorclass=DictCursor
    )
    logger.info("创建连接成功!")
    return conn


class MysqlPool(object):
    def __init__(self, max_conn=5):
        self.max_conn = max_conn

    def get_conn(self, db_name):
        db_configs = current_app.config['DATABASES']
        db_config = db_configs[db_name]
        """从连接池中获取链接"""
        if pool.qsize() < 1:
            new_conn = create_conn(db_config)
            pool.put(new_conn)
        try:
            conn = pool.get(timeout=30)
            conn.ping(True)
        except:
            conn = create_conn(db_config)
        return conn

    def release_conn(self, conn):
        """归还连接池"""
        pool.put(conn)
        print('连接池归还啦！当前总连接数：', pool.qsize())
