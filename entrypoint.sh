#!/usr/bin/sh
# @author by wangcw
# @generate at 2023-06-20 10:58:04

# 后台启动Celery
# nohup celery -A wsgi_gunicorn:celery_app worker -f ./logs/celery.log -l INFO &
# 启动FlaskAPP
gunicorn -c config/gun.conf wsgi_gunicorn:app

# nohup gunicorn -c config/gun.conf wsgi_gunicorn:app
