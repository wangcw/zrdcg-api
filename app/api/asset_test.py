#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/21 10:43


from flask import Blueprint, request, current_app
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from app.utils.util import route, MySQLUtil
from app.utils.database import ArgsUtils
from app.utils.auth import Auth, login_required
import json
import logging
import ast
# from app.celery import add, flask_app_context
import urllib.parse

bp = Blueprint("product", __name__, url_prefix='/')
logger = logging.getLogger(__name__)

@route(bp, '/zrdcg/v1/asset/getinstance', methods=["GET", "POST"])
def getinstance():

    res = ResMsg()

    inids = ArgsUtils.get_args('instanceid')
    page = ArgsUtils.get_args('page')
    page_size = ArgsUtils.get_args('page_size') or current_app.config['DEFAULT_PAGE_SIZE']
    in_clause = ArgsUtils.get_in_args(inids)

    sql_text = f'SELECT * \
    FROM meta_instance \
    WHERE InstanceId IN ({in_clause}) \
    LIMIT {page}, {page_size};'
    params = dict(in_clause=in_clause, page=page, page_size=page_size)

    db = MySQLUtil('SRC_ASSET_DB')
    try:
        result = db.fetch_all(sql_text, params)
        res.update(data=result)
    except Exception as e:
        result = dict(ErrType = 'Sql Execute Error', ErrMsg = str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
        logger.error("SQL语句执行失败！")

    return res.data


@route(bp, '/zrdcg/v1/asset/getinsurl', methods=["GET"])
def getinsurl():
    instanceid = ['i-bp183u4jakb6cyffc1tm', 'i-bp19eiphxl1np79slzam', 'pgm-bp13925lu2m16410']
    page = 0
    params = {
        'instanceid': instanceid,
        'page': page
    }
    query_string = urllib.parse.urlencode(params, quote_via=urllib.parse.quote)
    url = f"http://localhost:8093/zrdcg/v1/asset/getinstance?{query_string}"
    return url