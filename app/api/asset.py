#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/15 17:11

from flask import Blueprint, request, current_app
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from app.utils.util import route
from app.utils.auth import auth
from app.utils.oplogger import OperateLogger

bp = Blueprint("asset", __name__, url_prefix='/zrdcg/v1/asset/')
dblog = OperateLogger()


@route(bp, '/GetAssetList', methods=["POST"])
@auth.login_required
def getassetlist():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        asset_name = req_data['AssetName']
        asset_status = req_data['AssetStatus']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = 'SELECT * FROM vi_asset_info WHERE 1=1 '

    params = []
    if isinstance(asset_name, str) and asset_name is not None:
        sql_text += "AND `资产名称` LIKE %s "
        params.append('%' + asset_name + '%')
    elif asset_name is None:
        pass
    else:
        result = dict(error='参数AssetName传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(asset_status, str):
        sql_text += "AND `资产状态` = %s "
        params.append(asset_status)
    elif asset_status is None:
        pass
    else:
        result = dict(error='参数AssetStatus传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text += "ORDER BY Id "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(int(page_size))
    elif page_number is None:
        pass
    else:
        result = dict(error='参数page传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_asset = current_app.config['po_asset'].get_connection()
        cur_asset = con_asset.cursor(dictionary=True)
        cur_asset.execute(sql_text, params)
        result = cur_asset.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_asset.close()
        con_asset.close()
    return res.data


@route(bp, '/GetAssetInfo', methods=["POST"])
@auth.login_required
def getassetinfo():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        asset_id = req_data['AssetId']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    if not asset_id:
        result = dict(error='参数AssetId为空！')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    else:
        sql_asset = 'SELECT * FROM tb_asset_info WHERE Id = %s AND Deleted = 0;'

    try:
        con_asset = current_app.config['po_asset'].get_connection()
        cur_asset = con_asset.cursor(dictionary=True)
        cur_asset.execute(sql_asset, [asset_id])
        result = cur_asset.fetchall()
        if result:
            sql_ins = 'SELECT * FROM tb_asset_instance WHERE AssetId = %s AND Deleted = 0;'
            cur_asset.execute(sql_ins, [asset_id])
            res_ins = cur_asset.fetchall()
            if res_ins:
                for row in res_ins:
                    db_info = []
                    ins_id = row.get('Id')
                    sql_db = 'SELECT * FROM tb_asset_database WHERE AssetInsId = %s AND Deleted = 0;'
                    cur_asset.execute(sql_db, [ins_id])
                    res_db = cur_asset.fetchall()
                    if res_db:
                        row.update({"DatabaseInfo": res_db})
                result[0].update({"InstanceInfo": res_ins})
        else:
            res.update(code=ResponseCode.AssetNotExist, msg=ResponseMessage.AssetNotExist)
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_asset.close()
        con_asset.close()
    return res.data


@route(bp, '/GetInstanceList', methods=["POST"])
@auth.login_required
def getinstancelist():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        instance_id_desc = req_data['InstanceIdOrDesc']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = "SELECT * \
    FROM vi_dcg_instance \
    WHERE 1=1 "

    params = []
    if isinstance(instance_id_desc, str) and instance_id_desc is not None:
        sql_text += "AND (`实例Id` LIKE %s OR `实例描述` LIKE %s) "
        params.append('%' + instance_id_desc + '%')
        params.append('%' + instance_id_desc + '%')
    elif instance_id_desc is None:
        pass
    else:
        result = dict(error='参数InstanceIdOrDesc传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    # sql_text += "ORDER BY CreateTime DESC "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(int(page_size))
    elif page_number is None:
        pass
    else:
        result = dict(error='参数PageNumber传输错误！')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_src = current_app.config['po_src'].get_connection()
        cur_src = con_src.cursor(dictionary=True)
        cur_src.execute(sql_text, params)
        result = cur_src.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_src.close()
        con_src.close()
    return res.data


@route(bp, '/GetInstanceDBList', methods=["POST"])
@auth.login_required
def getinstancedblist():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        instance_id = req_data['InstanceId']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = 'SELECT DBName as `库名`, DBDesc as `库备注`, GROUP_CONCAT(b.Account) as `账号(请保留一个)` ' \
               'FROM meta_instancedatabase a, JSON_TABLE(a.AccountPrivilege, \'$[*]\' COLUMNS (' \
               'Account VARCHAR(50) PATH \'$.Account\' )) AS b WHERE 1 = 1 '
    params = []
    if isinstance(instance_id, str) and instance_id is not None:
        sql_text += "AND InstanceId = %s "
        params.append(instance_id)
    elif instance_id is None:
        pass
    else:
        result = dict(error='参数InstanceId传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text += "AND Deleted = 0 GROUP BY a.InstanceId,a.DBName ORDER BY a.Id DESC "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(int(page_size))
    elif page_number is None:
        pass
    else:
        result = dict(error='参数PageNumber传输错误！')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_src = current_app.config['po_src'].get_connection()
        cur_src = con_src.cursor(dictionary=True)
        cur_src.execute(sql_text, params)
        result = cur_src.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_src.close()
        con_src.close()
    return res.data


@route(bp, '/AddAssetInfo', methods=["POST"])
@auth.login_required
def addassetinfo():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        asset_name = req_data['AssetName']
        instance_info = req_data['InstanceInfo']
        instance_id = instance_info['InstanceId']
        db_info = instance_info['DBInfo']
        extraction_rules = req_data['ExtractionRules']
        data_concurrency = req_data['DataConcurrency']
        execution_crontab = req_data['ExecutionCrontab']
        dat_authority = req_data['DatAuthority']
        develop_users = req_data['DevelopUsers']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    con_src = current_app.config['po_src'].get_connection()
    con_asset = current_app.config['po_asset'].get_connection()
    con_task = current_app.config['po_task'].get_connection()

    cur_src = con_src.cursor(dictionary=True)
    cur_asset = con_asset.cursor(dictionary=True)
    cur_task = con_task.cursor(dictionary=True)

    try:
        # 资产列表信息写入
        # 判断实例名称是否存在
        sql_asset_name_exists = 'SELECT Id \
        FROM tb_asset_info \
        WHERE AssetName = %s \
        AND Deleted = 0;'

        cur_asset.execute(sql_asset_name_exists, [asset_name])
        res_asset_name = cur_asset.fetchone()
        if res_asset_name:
            res.update(code=ResponseCode.AssetNameExists, msg=ResponseMessage.AssetNameExists)
            return res.data
        # 获取产品线
        sql_production_unit = 'SELECT ProductionUnit, ProductLine \
        FROM meta_instancesec \
        WHERE Deleted = 0 \
          AND InstanceId = %s;'
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['AI'])
        res_cur = cur_task.fetchone()
        if res_cur:
            asset_id = res_cur.get('Id')
        cur_src.execute(sql_production_unit, [instance_id])
        res_cur = cur_src.fetchone()
        if res_cur:
            product_unit = res_cur.get('ProductionUnit')
        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        sql_asset = 'INSERT INTO tb_asset_info(Id, ProductionUnit, AssetName, AssetStatus, DataOrigin, \
        ExtractionRules, DataConcurrency, StorageMode, ExecutionCrontab, DatAuthority, \
        DevelopUsers, CreatedById, UpdatedbyId) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);'
        params_asset = [asset_id, product_unit, asset_name, 0, 1, extraction_rules, data_concurrency, 2,
                        execution_crontab, dat_authority, develop_users, current_user_id, current_user_id]
        cur_asset.execute(sql_asset, params_asset)

        # 写入实例信息
        sql_src_ins = 'SELECT InstanceId, ConnectionString AS DBAddress, Port AS DBPort, Engine AS InstanceType \
        FROM meta_dmsinstance \
        WHERE Deleted = 0 \
        AND InstanceId = %s;'
        cur_src.execute(sql_src_ins, [instance_id])
        res_ins = cur_src.fetchone()
        if res_ins:
            instance_type = res_ins.get('InstanceType')
            db_address = res_ins.get('DBAddress')
            db_port = res_ins.get('DBPort')
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['AD'])
        res_cur = cur_task.fetchone()
        if res_cur:
            instance_asset_id = res_cur.get('Id')

        db_name_concat = ','.join([d['DBName'] for d in db_info])
        params_ins = [instance_asset_id, asset_id, instance_id, instance_type, db_address, db_port,
                      1, 2, db_name_concat, current_user_id, current_user_id]
        sql_ins = 'INSERT INTO zr_dcg_asset.tb_asset_instance(Id, AssetId, InstanceId, InstanceType, DBAddress, DBPort, IsStructure, \
        DataSources, DBNames, CreatedById, UpdatedbyId) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
        cur_asset.execute(sql_ins, params_ins)
        # 写入数据库信息
        params_db = []
        for db in db_info:
            db_name = db['DBName']
            db_desc = db['DBDesc']
            user_name = db['UserName']
            user_pwd = db['UserPwd']
            cur_task.execute('SELECT fn_nextval(%s) AS Id', ['DD'])
            res_cur = cur_task.fetchone()
            if res_cur:
                instance_db_id = res_cur.get('Id')
            params_db.append([instance_db_id, asset_id, instance_asset_id, db_name, db_desc,
                              user_name, user_pwd, current_user_id, current_user_id])

        sql_db = 'INSERT INTO tb_asset_database(Id, AssetId, AssetInsId, DBName, DBDesc, UserName, \
        UserPwd, CreatedById, UpdatedbyId) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'

        cur_asset.executemany(sql_db, params_db)
    # 统一提交事务
        con_task.commit()
        con_asset.commit()

    except Exception as e:
        con_asset.rollback()
        # con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
        return res.data
    finally:
        cur_src.close()
        cur_asset.close()
        cur_task.close()
        con_src.close()
        con_asset.close()
        con_task.close()
    dbloginfo = '用户' + str(current_user) + '新了增资产：' + str(asset_name)
    dblog.operate_log('资产', dbloginfo)
    return res.data


@route(bp, '/UpdateAssetInfo', methods=["POST"])
@auth.login_required
def updateassetinfo():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        asset_id = req_data['AssetId']
        # asset_ins_id = req_data['AssetInsId']
        asset_name = req_data['AssetName']
        instance_info = req_data['InstanceInfo']
        instance_id = instance_info['InstanceId']
        db_info = instance_info['DBInfo']
        extraction_rules = req_data['ExtractionRules']
        data_concurrency = req_data['DataConcurrency']
        execution_crontab = req_data['ExecutionCrontab']
        dat_authority = req_data['DatAuthority']
        develop_users = req_data['DevelopUsers']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    con_src = current_app.config['po_src'].get_connection()
    con_asset = current_app.config['po_asset'].get_connection()
    con_task = current_app.config['po_task'].get_connection()
    cur_src = con_src.cursor(dictionary=True)
    cur_asset = con_asset.cursor(dictionary=True)
    cur_task = con_task.cursor(dictionary=True)

    try:
        # 资产列表信息更新
        # 判断资产是否存在
        sql_asset_id_extsts = 'SELECT Id \
        FROM zr_dcg_asset.tb_asset_info \
        WHERE Id = %s \
        AND Deleted = 0;'

        cur_asset.execute(sql_asset_id_extsts, [asset_id])
        res_asset_id = cur_asset.fetchone()
        if not res_asset_id:
            res.update(code=ResponseCode.AssetNotExist, msg=ResponseMessage.AssetNotExist)
            return res.data
        # 获取产品线
        sql_production_unit = 'SELECT ProductionUnit, ProductLine \
        FROM meta_instancesec \
        WHERE Deleted = 0 \
          AND InstanceId = %s;'
        cur_src.execute(sql_production_unit, [instance_id])
        res_cur = cur_src.fetchone()
        if res_cur:
            product_unit = res_cur.get('ProductionUnit')

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')

        # 获取原写入时间
        sql_asset_createinfo = 'SELECT AssetName, CreatedById, CreatedAt, ExecutionCrontab \
                         FROM tb_asset_info  \
                         WHERE Id = %s'
        cur_asset.execute(sql_asset_createinfo, [asset_id])
        res_cur = cur_asset.fetchone()
        if res_cur:
            src_asset_name = res_cur.get('AssetName')
            createbyid_asset = res_cur.get('CreatedById')
            createat_asset = res_cur.get('CreatedAt')
            # execution_crontab_asset = res_cur.get('ExecutionCrontab')

        # 判断更新的资产名称是否等于新名称
        if src_asset_name == asset_name:
            res.update(code=ResponseCode.AssetNameExists, msg=ResponseMessage.AssetNameExists)
            return res.data

        # 更新数据
        sql_asset = 'REPLACE INTO tb_asset_info(Id, ProductionUnit, AssetName, DataOrigin, \
        ExtractionRules, DataConcurrency, StorageMode, ExecutionCrontab, DatAuthority, DevelopUsers, \
        CreatedById, CreatedAt, UpdatedbyId) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);'
        params_asset = [asset_id, product_unit, asset_name, 1, extraction_rules, data_concurrency, 2,
                        execution_crontab, dat_authority, develop_users, createbyid_asset,
                        createat_asset, current_user_id]
        cur_asset.execute(sql_asset, params_asset)

        # 更新定时任务
        # if execution_crontab_asset != execution_crontab:
        #     schedule_update(collect_instance, [asset_id], asset_id, asset_name, execution_crontab)

        # 更新实例信息
        sql_src_ins = 'SELECT InstanceId, ConnectionString AS DBAddress, Port AS DBPort, Engine AS InstanceType \
        FROM meta_dmsinstance \
        WHERE Deleted = 0 \
        AND InstanceId = %s;'

        cur_src.execute(sql_src_ins, [instance_id])
        res_ins = cur_src.fetchone()
        if res_ins:
            instance_type = res_ins.get('InstanceType')
            db_address = res_ins.get('DBAddress')
            db_port = res_ins.get('DBPort')

        #获取实例Id
        sql_asset_ins_id = 'SELECT Id FROM tb_asset_instance WHERE AssetId = %s AND Deleted = 0 LIMIT 1;'
        cur_asset.execute(sql_asset_ins_id, [asset_id])
        res_asset_ins_id = cur_asset.fetchone()
        if res_asset_ins_id:
            asset_ins_id = res_asset_ins_id.get('Id')

        db_name_concat = ','.join([d['DBName'] for d in db_info])
        params_ins = [asset_ins_id, asset_id, instance_id, instance_type, db_address, db_port,
                      1, 2, db_name_concat, createbyid_asset, createat_asset, current_user_id]
        sql_ins = 'REPLACE INTO tb_asset_instance(Id, AssetId, InstanceId, InstanceType, DBAddress, DBPort, IsStructure, \
        DataSources, DBNames, CreatedById, CreatedAt, UpdatedbyId) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
        cur_asset.execute(sql_ins, params_ins)

        # 取出数据库中所有库信息
        sql_db_exists = 'SELECT DBName, DBDesc, UserName, UserPwd \
        FROM tb_asset_database \
        WHERE Deleted = 0 \
          AND AssetId = %s'
        cur_asset.execute(sql_db_exists, [asset_id])
        asset_dbs = cur_asset.fetchall()

        dbs_upd = [j for i in asset_dbs for j in db_info if i['DBName'] == j['DBName']]
        dbs_del = [i for i in asset_dbs if not any(i['DBName'] == j['DBName'] for j in db_info)]
        dbs_ins = [i for i in db_info if not any(i['DBName'] == j['DBName'] for j in asset_dbs)]
        for db in dbs_ins:
            cur_task.execute('SELECT fn_nextval(%s) AS Id', ['DD'])
            res_cur = cur_task.fetchone()
            ins_db_id = res_cur.get('Id')
            db_name = db.get('DBName')
            db_desc = db.get('DBDesc')
            db_username = db.get('UserName')
            db_userpwd = db.get('UserPwd')
            sql_db_ins = 'INSERT INTO tb_asset_database(Id, AssetId, AssetInsId, DBName, DBDesc, UserName, \
               UserPwd, CreatedById, UpdatedbyId) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'
            cur_asset.execute(sql_db_ins, [ins_db_id, asset_id, asset_ins_id, db_name, db_desc,
                                           db_username, db_userpwd ,current_user_id, current_user_id])
        for db in dbs_del:
            db_name = db.get('DBName')
            sql_db_del = 'UPDATE tb_asset_database ' \
                         'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                         'WHERE AssetId = %s ' \
                         'AND DBName = %s ' \
                         'AND Deleted = 0;'
            cur_asset.execute(sql_db_del, [current_user_id, asset_id, db_name])
        for db in dbs_upd:
            db_name = db.get('DBName')
            db_desc = db.get('DBDesc')
            db_username = db.get('UserName')
            db_userpwd = db.get('UserPwd')
            sql_db_upd = 'UPDATE tb_asset_database ' \
                         'SET DBDesc = %s, ' \
                         'UserName = %s, ' \
                         'UserPwd = %s, ' \
                         'UpdatedbyId = %s ' \
                         'WHERE AssetId = %s ' \
                         'AND DBName = %s ' \
                         'AND Deleted = 0;'
            cur_asset.execute(sql_db_upd, [db_desc, db_username, db_userpwd, current_user_id,
                                           asset_id, db_name])

        # 统一提交事务
        con_asset.commit()
        con_task.commit()
    except Exception as e:
        con_asset.rollback()
        # con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
        return res.data
    finally:
        cur_src.close()
        cur_asset.close()
        cur_task.close()
        con_src.close()
        con_asset.close()
        con_task.close()
    dbloginfo = '用户' + str(current_user) + '更新了资产：' + str(asset_name)
    dblog.operate_log('资产', dbloginfo)
    return res.data


@route(bp, '/DeleteAssetInfo', methods=["POST"])
@auth.login_required
def deleteassetinfo():
    try:
        res = ResMsg()
        req_data = request.get_json(force=True)
        asset_id = req_data['AssetId']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    con_asset = current_app.config['po_asset'].get_connection()
    con_task = current_app.config['po_task'].get_connection()

    cur_asset = con_asset.cursor(dictionary=True)
    cur_task = con_task.cursor(dictionary=True)

    try:
        # 判断资产是否存在
        sql_asset_id_extsts = 'SELECT Id \
        FROM zr_dcg_asset.tb_asset_info \
        WHERE Id = %s \
        AND Deleted = 0;'

        cur_asset.execute(sql_asset_id_extsts, [asset_id])
        res_asset_id = cur_asset.fetchone()
        if not res_asset_id:
            res.update(code=ResponseCode.AssetNotExist, msg=ResponseMessage.AssetNotExist)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')

        sql_asset_del = 'UPDATE tb_asset_info SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 WHERE Id = %s;'
        sql_ins_del = 'UPDATE tb_asset_instance SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 WHERE Id = %s;'
        sql_db_del = 'UPDATE tb_asset_database SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 WHERE AssetId = %s;'
        cur_asset.execute(sql_asset_del, [current_user_id, asset_id])
        cur_asset.execute(sql_ins_del, [current_user_id, asset_id])
        cur_asset.execute(sql_db_del, [current_user_id, asset_id])

    # 统一提交事务
        con_asset.commit()
    except Exception as e:
        con_asset.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
        return res.data
    finally:
        cur_asset.close()
        cur_task.close()
        con_asset.close()
        con_task.close()
    dbloginfo = '用户' + str(current_user) + '删除了资产：' + str(asset_id)
    dblog.operate_log('资产', dbloginfo)
    return res.data
