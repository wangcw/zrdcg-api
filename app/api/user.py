#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/28 08:58

from flask import Blueprint, request, current_app
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from app.utils.util import route
from app.utils.auth import auth, hash_password
from app.utils.oplogger import OperateLogger

bp = Blueprint("user", __name__, url_prefix='/')
dblog = OperateLogger()


@route(bp, '/zrdcg/v1/AddUser', methods=["POST"])
@auth.login_required
def adduser():
    """新增用户"""
    res = ResMsg()
    obj = request.get_json(force=True)
    user_name = obj.get("user_name")
    pass_word = obj.get("pass_word")
    if not obj or not user_name:
        res.update(code=ResponseCode.UserOrPasswdIsEmpty, msg=ResponseMessage.UserOrPasswdIsEmpty)
        return res.data
    if len(pass_word) < 6:
        res.update(code=ResponseCode.PassWordLessThan6, msg=ResponseMessage.PassWordLessThan6)
        return res.data
    sql_have_user = 'SELECT 1 AS IsHave  FROM tb_basic_user WHERE UserName = %s AND Deleted=0;'
    sql_get_id = 'SELECT fn_nextval(%s) AS Id;'
    sql_add_user = 'INSERT INTO tb_basic_user(Id, UserName, PassWord, \
        CreatedById, UpdatedbyId) VALUES(%s, %s, %s, %s, %s);'
    sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_have_user, [user_name])
        if cur_task.fetchone():
            res.update(code=ResponseCode.UserHaveFond, msg=ResponseMessage.UserHaveFond)
            return res.data
        cur_task.execute(sql_get_id, ['BU'])
        user_id = cur_task.fetchone()
        if user_id:
            user_id = user_id.get('Id')
        password_hash = hash_password(pass_word)
        current_user = auth.current_user()
        cur_task.execute(sql_user_id, [current_user])
        current_user_id = cur_task.fetchone().get('Id')
        cur_task.execute(sql_add_user, [user_id, user_name, password_hash, current_user_id, current_user_id])
        con_task.commit()
        dbloginfo = '用户' + str(user_name) + '新增成功！'
        dblog.operate_log('用户', dbloginfo)
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
        return res.data
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/zrdcg/v1/ChangePasswd', methods=["POST"])
@auth.login_required
def changepasswd():
    """修改用户密码"""
    res = ResMsg()
    obj = request.get_json(force=True)
    user_name = obj.get("user_name")
    new_pass_word = obj.get("new_pass_word")
    if not obj or not user_name:
        res.update(code=ResponseCode.UserOrPasswdIsEmpty, msg=ResponseMessage.UserOrPasswdIsEmpty)
        return res.data
    if len(new_pass_word) < 6:
        res.update(code=ResponseCode.PassWordLessThan6, msg=ResponseMessage.PassWordLessThan6)
        return res.data

    sql_user_status = 'SELECT UserStatus FROM tb_basic_user WHERE Id = %s AND Deleted = 0;'
    sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
    sql_change_passwd = 'UPDATE tb_basic_user SET PassWord = %s,LastChangePwd = NOW(), UpdatedById = %s WHERE Id = %s;'

    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_user_id, [user_name])
        user_id = cur_task.fetchone().get('Id')
        cur_task.execute(sql_user_status, [user_id])
        user_status = cur_task.fetchone()
        if not user_status:
            res.update(code=ResponseCode.UserNotFond, msg=ResponseMessage.UserNotFond)
            return res.data
        elif user_status.get('UserStatus') == 2:
            res.update(code=ResponseCode.UserIsLocked, msg=ResponseMessage.UserIsLocked)
            return res.data
        current_user = auth.current_user()
        cur_task.execute(sql_user_id, [current_user])
        current_user_id = cur_task.fetchone().get('Id')
        password_hash = hash_password(new_pass_word)
        cur_task.execute(sql_change_passwd, [password_hash, current_user_id, user_id])
        con_task.commit()
        dbloginfo = '用户' + str(user_name) + '密码修改成功！'
        dblog.operate_log('用户', dbloginfo)
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/zrdcg/v1/DeleteUser', methods=["POST"])
@auth.login_required
def deleteuser():
    """用户删除"""
    res = ResMsg()
    obj = request.get_json(force=True)
    user_name = obj.get("user_name")
    if not obj or not user_name:
        res.update(code=ResponseCode.UserOrPasswdIsEmpty, msg=ResponseMessage.UserOrPasswdIsEmpty)
        return res.data

    sql_user_status = 'SELECT UserStatus FROM tb_basic_user WHERE Id = %s AND Deleted = 0;'
    sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
    sql_user_delete = 'UPDATE tb_basic_user SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 WHERE Id = %s;'

    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_user_id, [user_name])
        user_id = cur_task.fetchone().get('Id')
        cur_task.execute(sql_user_status, [user_id])
        user_status = cur_task.fetchone()
        if not user_status:
            res.update(code=ResponseCode.UserNotFond, msg=ResponseMessage.UserNotFond)
            return res.data
        current_user = auth.current_user()
        cur_task.execute(sql_user_id, [current_user])
        current_user_id = cur_task.fetchone().get('Id')
        cur_task.execute(sql_user_delete, [current_user_id, user_id])
        con_task.commit()
        dbloginfo = '用户' + str(user_name) + '已删除！'
        dblog.operate_log('用户', dbloginfo)
    except Exception as e:
        con_task.rollback()
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/zrdcg/v1/UnlockUser', methods=["POST"])
@auth.login_required
def unlockuser():
    """用户解锁"""
    res = ResMsg()
    obj = request.get_json(force=True)
    user_name = obj.get("user_name")
    if not obj or not user_name:
        res.update(code=ResponseCode.UserOrPasswdIsEmpty, msg=ResponseMessage.UserOrPasswdIsEmpty)
        return res.data

    sql_have_user = 'SELECT 1 AS IsHave  FROM tb_basic_user WHERE UserName = %s AND Deleted=0;'
    sql_user_unlock = 'UPDATE tb_basic_user SET UserStatus = 2, LoginFailnums = %s, LockBeginTime = NOW() ' \
                      'WHERE UserName = %s AND Deleted = 0;'

    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_have_user, [user_name])
        if not cur_task.fetchone():
            res.update(code=ResponseCode.UserNotFond, msg=ResponseMessage.UserNotFond)
            return res.data
        cur_task.execute(sql_user_unlock, [user_name])
        con_task.commit()
        dbloginfo = '用户' + str(user_name) + '已解锁！'
        dblog.operate_log('用户', dbloginfo)
    except Exception as e:
        con_task.rollback()
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError)
    finally:
        cur_task.close()
        con_task.close()
    return res.datas
