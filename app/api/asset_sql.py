#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/6/15 17:11

from flask import Blueprint, request, current_app
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from app.utils.util import route
from app.utils.auth import auth
from app.utils.oplogger import OperateLogger

bp = Blueprint("asset_sql", __name__, url_prefix='/')
dblog = OperateLogger()


@route(bp, '/zrdcg/v1/asset/GetAssetList', methods=["POST"])
# @auth.login_required
def getassetlist():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        asset_name = req_data['AssetName']
        asset_status = req_data['AssetStatus']
        page_number = req_data['PageNumber']
        page_size = req_data['PageSize'] or current_app.config['DEFAULT_PAGE_SIZE']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data

    sql_text = "SELECT * \
    FROM tb_asset_info \
    WHERE 1=1 "

    params = []
    if isinstance(asset_name, str) and asset_name is not None:
        sql_text += "AND AssetName LIKE %s "
        params.append('%' + asset_name + '%')
    elif asset_name is None:
        pass
    else:
        result = dict(error='参数AssetName传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(asset_status, int):
        sql_text += "AND AssetStatus = %s "
        params.append(asset_status)
    elif asset_status is None:
        pass
    else:
        result = dict(error='参数AssetStatus传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text += "AND Deleted = 0 \
    ORDER BY Id "
    if isinstance(page_number, int) and page_number is not None:
        sql_text += "LIMIT %s, %s;"
        page_number = int(page_number) - 1
        if page_number < 0:
            page_number = 0
        params.append(page_number)
        params.append(page_size)
    elif page_number is None:
        pass
    else:
        result = dict(error='参数page传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_src = current_app.config['po_asset'].get_connection()
        cur_src = con_src.cursor(dictionary=True)
        cur_src.execute(sql_text, params)
        result = cur_src.fetchall()
        res.update(data=result)
    except Exception as e:
        print(e)
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_src.close()
        con_src.close()
    return res.data
