#!/usr/bin/sh
# @author by wangcw
# @generate at 2023-07-06 14:58:01

# 修改flask中json格式化时默认排序
vim $env_home/envs/zrdcg/lib/python3.8/site-packages/flask/json/provider.py
# 修改 151行 内容，将True改为False
sort_keys = False

### 已重写jsonify修改默认参数修复
