#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/8/4 11:53

from flask import Blueprint, request, current_app
from app.utils.code import ResponseCode, ResponseMessage
from app.utils.response import ResMsg
from app.utils.util import route
from app.utils.auth import auth
from app.utils.oplogger import OperateLogger
from app.api.tree import TempClassTree
import json

bp = Blueprint("template", __name__, url_prefix='/zrdcg/v1/template/')
dblog = OperateLogger()

# 设置公用方法，更新目录数量
sql_tem_cnt = 'WITH RECURSIVE cte AS ( SELECT Id, TempClassPId FROM tb_template_class WHERE Id=%s ' \
              'UNION ALL SELECT t.Id, t.TempClassPId FROM tb_template_class t ' \
              'JOIN cte c ON c.TempClassPId = t.Id ) UPDATE tb_template_class x, cte y, ' \
              '(SELECT TempClassId, COUNT(1) AS TemplateCount FROM tb_template_detail ' \
              'WHERE TempClassId=%s AND Deleted=0) z SET x.FeatureCount=z.TemplateCount WHERE x.Id=y.Id ' \
              'AND x.Id=y.TempClassPId AND x.Deleted=0;'

sql_level = 'WITH RECURSIVE cte AS ( SELECT Id, TempClassPId FROM tb_template_class WHERE Id = %s ' \
            'UNION ALL SELECT t.Id, t.TempClassPId FROM tb_template_class t ' \
            'JOIN cte c ON c.TempClassPId = t.Id ) ' \
            'SELECT COUNT(1) AS ClassCount FROM cte;'


@route(bp, '/AddTempInfo', methods=["POST"])
@auth.login_required
def addti():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_name = req_data['TemplateName']
        temp_sta = req_data['TemplateStatus']
        if not temp_name:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证模板名称是否存在
        sql_name_exists = 'SELECT Id FROM tb_template_info ' \
                          'WHERE TemplateName = %s AND Deleted = 0;'

        cur_task.execute(sql_name_exists, [temp_name])
        res_temp_name = cur_task.fetchone()
        if res_temp_name:
            res.update(code=ResponseCode.TempNameExists, msg=ResponseMessage.TempNameExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        # nextval
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['TI'])
        res_cur = cur_task.fetchone()
        temp_id = res_cur.get('Id')

        sql_temp_ins = 'insert into tb_template_info (Id, TemplateName, ' \
                       'TemplateStatus, CreatedById, UpdatedbyId) ' \
                       'values (%s, %s, %s, %s, %s);'
        params_class = [temp_id, temp_name, temp_sta, current_user_id, current_user_id]

        cur_task.execute(sql_temp_ins, params_class)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelTempInfo', methods=["POST"])
@auth.login_required
def delti():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_id = req_data['TemplateId']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # temp_id必填
        if not temp_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data

        # 验证模版是否存在
        sql_id_exists = 'SELECT Id \
        FROM tb_template_info \
        WHERE Id = %s \
        AND Deleted = 0;'

        cur_task.execute(sql_id_exists, [temp_id])
        res_temp_id = cur_task.fetchall()
        if not res_temp_id:
            res.update(code=ResponseCode.TempIdNotExists, msg=ResponseMessage.TempIdNotExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_temp_del = 'UPDATE tb_template_info ' \
                       'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                       'WHERE Id = %s;'

        cur_task.execute(sql_temp_del, [current_user_id, temp_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateTempInfo', methods=["POST"])
@auth.login_required
def updateti():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_id = req_data['TemplateId']
        temp_name = req_data['TemplateName']
        temp_sta = req_data['TemplateStatus']

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # temp_id 必填
        if not temp_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data

        # 验证模板Id是否存在
        sql_id_exists = 'SELECT Id, TemplateName, TemplateStatus \
        FROM tb_template_info \
        WHERE Id = %s \
        AND Deleted = 0;'

        cur_task.execute(sql_id_exists, [temp_id])
        res_temp = cur_task.fetchone()
        if not res_temp:
            res.update(code=ResponseCode.TempIdNotExists, msg=ResponseMessage.TempIdNotExists)
            return res.data
        else:
            res_temp_name = res_temp.get('TemplateName')
            res_temp_sta = res_temp.get('TemplateStatus')

        # 验证模板名称是否存在
        if temp_name:
            sql_name_exists = 'SELECT 1 FROM tb_template_info ' \
                              'WHERE TemplateName = %s AND Id!=%s AND Deleted = 0;'

            cur_task.execute(sql_name_exists, [temp_name, temp_id])
            res_temp = cur_task.fetchone()
            if res_temp:
                res.update(code=ResponseCode.TempNameExists, msg=ResponseMessage.TempNameExists)
                return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_temp_update = 'UPDATE tb_template_info ' \
                          'SET UpdatedbyId = %s '
        if res_temp_name != temp_name:
            sql_temp_update += ',TemplateName = %s '
            params_update.append(temp_name)
        if res_temp_sta != temp_sta:
            sql_temp_update += ',TemplateStatus = %s '
            params_update.append(temp_sta)
        sql_temp_update += 'WHERE Id = %s;'
        params_update.append(temp_id)
        cur_task.execute(sql_temp_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetTempInfo', methods=["POST"])
@auth.login_required
def getti():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_id = req_data['TemplateId']
        temp_name = req_data['TemplateName']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text = 'SELECT * FROM tb_template_info a WHERE 1=1 '
    params = []
    if isinstance(temp_id, str):
        sql_text += "AND Id = %s "
        params.append(temp_id)
    elif temp_id is None:
        pass
    else:
        result = dict(error='参数Id传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(temp_name, str):
        sql_text += "AND TemplateName LIKE %s "
        params.append('%' + temp_name + '%')
    elif temp_name is None:
        pass
    else:
        result = dict(error='参数Id传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/AddTempClass', methods=["POST"])
@auth.login_required
def addtc():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_id = req_data['TemplateId']
        class_path = req_data['TempClassPath']
        class_name = req_data['TempClassName']
        class_pid = req_data['TempClassPId']
        fclass_id = req_data['FeatureClassId']
        level_id = req_data['LevelId']
        class_index = req_data['ClassIndex']
        class_remark = req_data['ClassRemark']
        if not temp_id or not class_name:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证模板Id是否存在
        sql_id_exists = 'SELECT Id FROM tb_template_info WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_id_exists, [temp_id])
        res_temp_id = cur_task.fetchall()
        if not res_temp_id:
            res.update(code=ResponseCode.TempIdNotExists, msg=ResponseMessage.TempIdNotExists)
            return res.data

        # 验证分类目录Id是否存在
        sql_feature_class_id_exists = 'SELECT Id FROM tb_feature_class WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_feature_class_id_exists, [fclass_id])
        res_feature_class_id = cur_task.fetchall()
        if not res_feature_class_id:
            res.update(code=ResponseCode.ClassIdNotExists, msg=ResponseMessage.ClassIdNotExists)
            return res.data

        # 验证分级Id是否存在
        sql_level_id_exists = 'SELECT Id FROM tb_data_level WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_level_id_exists, [level_id])
        res_level_id = cur_task.fetchall()
        if not res_level_id:
            res.update(code=ResponseCode.LevelIdNotExists, msg=ResponseMessage.LevelIdNotExists)
            return res.data

        # 验证同一级下模板分类名称是否存在
        sql_name_exists = 'SELECT Id FROM tb_template_class ' \
                          'WHERE TempClassName = %s AND TempClassPId = %s AND Deleted = 0;'

        cur_task.execute(sql_name_exists, [class_name, class_pid])
        res_class_name = cur_task.fetchone()
        if res_class_name:
            res.update(code=ResponseCode.TempClassNameExists, msg=ResponseMessage.TempClassNameExists)
            return res.data

        # 验证上级分类目录是否存在
        if class_pid:
            sql_pid_exists = 'SELECT Id FROM tb_template_class WHERE Id = %s AND Deleted = 0;'

            cur_task.execute(sql_pid_exists, [class_pid])
            res_class_pid = cur_task.fetchone()
            if not res_class_pid:
                res.update(code=ResponseCode.TempClassIdNotExists, msg=ResponseMessage.TempClassIdNotExists)
                return res.data
        else:
            class_pid = None

        # 验证模板分类层级是否大于4
        cur_task.execute(sql_level, [class_pid])
        res_class_count = cur_task.fetchall()
        if res_class_count:
            res_class_count = res_class_count[0].get('ClassCount')
            if res_class_count > 3:
                res.update(code=ResponseCode.TempClassLevelMoreThan4, msg=ResponseMessage.TempClassLevelMoreThan4)
                return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        # nextval
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['TE'])
        res_cur = cur_task.fetchone()
        temp_class_id = res_cur.get('Id')

        sql_temp_class_ins = 'insert into tb_template_class (Id, TemplateId, TempClassPath, ' \
                             'TempClassName, TempClassPId, FeatureClassId, LevelId, ClassIndex, ' \
                             'ClassRemark, CreatedById, UpdatedbyId) ' \
                             'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);'
        params_class = [temp_class_id, temp_id, class_path, class_name, class_pid, fclass_id,
                        level_id, class_index, class_remark, current_user_id, current_user_id]

        cur_task.execute(sql_temp_class_ins, params_class)
        succ_id = dict(TempClassId=temp_class_id)
        res.update(data=succ_id)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelTempClass', methods=["POST"])
@auth.login_required
def deltc():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_class_id = req_data['TempClassId']
        # temp_class_id 必填
        if not temp_class_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证模板分类目录是否存在
        sql_id_exists = 'SELECT Id \
            FROM tb_template_class \
            WHERE Id = %s \
            AND Deleted = 0;'

        cur_task.execute(sql_id_exists, [temp_class_id])
        res_temp_class_id = cur_task.fetchall()
        if not res_temp_class_id:
            res.update(code=ResponseCode.TempClassIdNotExists, msg=ResponseMessage.TempClassIdNotExists)
            return res.data

        # 验证是否有下级分类
        sql_cid_exists = 'SELECT Id \
            FROM tb_template_class \
            WHERE TempClassPId = %s \
            AND Deleted = 0;'

        cur_task.execute(sql_cid_exists, [temp_class_id])
        res_temp_class_cid = cur_task.fetchall()
        if res_temp_class_cid:
            res.update(code=ResponseCode.TempClassChildExists, msg=ResponseMessage.TempClassChildExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_class_del = 'UPDATE tb_template_class ' \
                        'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                        'WHERE Id = %s;'

        cur_task.execute(sql_class_del, [current_user_id, temp_class_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateTempClass', methods=["POST"])
@auth.login_required
def updatetc():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        class_id = req_data['TempClassId']
        temp_id = req_data['TemplateId']
        class_path = req_data['TempClassPath']
        class_name = req_data['TempClassName']
        class_pid = req_data['TempClassPId']
        fclass_id = req_data['FeatureClassId']
        level_id = req_data['LevelId']
        class_index = req_data['ClassIndex']
        class_remark = req_data['ClassRemark']
        if not class_id:
            res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter)
            return res.data
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证模板Id是否存在
        sql_id_exists = 'SELECT Id \
                        FROM tb_template_info \
                        WHERE Id = %s \
                        AND Deleted = 0;'

        cur_task.execute(sql_id_exists, [temp_id])
        res_temp_id = cur_task.fetchall()
        if not res_temp_id:
            res.update(code=ResponseCode.TempIdNotExists, msg=ResponseMessage.TempIdNotExists)
            return res.data

        # 验证模板名称是否存在
        if class_name:
            sql_name_exists = 'SELECT Id FROM tb_template_info ' \
                              'WHERE Id != %s AND TemplateName = %s AND Deleted = 0;'

            cur_task.execute(sql_name_exists, [temp_id, class_name])
            res_temp_id = cur_task.fetchall()
            if not res_temp_id:
                res.update(code=ResponseCode.TempClassNameExists, msg=ResponseMessage.TempClassNameExists)
                return res.data

        # 验证分类目录Id是否存在
        sql_feature_class_id_exists = 'SELECT Id FROM tb_feature_class WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_feature_class_id_exists, [fclass_id])
        res_feature_class_id = cur_task.fetchall()
        if not res_feature_class_id:
            res.update(code=ResponseCode.ClassIdNotExists, msg=ResponseMessage.ClassIdNotExists)
            return res.data

        # 验证分级Id是否存在
        sql_level_id_exists = 'SELECT Id FROM tb_data_level WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_level_id_exists, [level_id])
        res_level_id = cur_task.fetchall()
        if not res_level_id:
            res.update(code=ResponseCode.LevelIdNotExists, msg=ResponseMessage.LevelIdNotExists)
            return res.data

        # 验证同一级下模板分类名称是否存在
        sql_name_exists = 'SELECT Id FROM tb_template_class ' \
                          'WHERE TempClassName = %s AND TempClassPId = %s AND Deleted = 0;'

        cur_task.execute(sql_name_exists, [class_name, class_pid])
        res_class_name = cur_task.fetchone()
        if res_class_name:
            res.update(code=ResponseCode.TempClassNameExists, msg=ResponseMessage.TempClassNameExists)
            return res.data

        # 验证模板分类目录是否存在
        sql_pid_exists = 'SELECT Id, TemplateId, TempClassPath, TempClassName, TempClassPId, ' \
                         'FeatureClassId, LevelId, ClassIndex, ClassRemark ' \
                         'FROM tb_template_class WHERE Id = %s AND Deleted = 0;'

        cur_task.execute(sql_pid_exists, [class_id])
        res_class = cur_task.fetchone()
        if not res_class:
            res.update(code=ResponseCode.TempClassIdNotExists, msg=ResponseMessage.TempClassIdNotExists)
            return res.data
        else:
            res_temp_id = res_class.get('TemplateId')
            res_class_path = res_class.get('TempClassPath')
            res_class_name = res_class.get('TempClassName')
            res_class_pid = res_class.get('TempClassPId')
            res_fclass_id = res_class.get('FeatureClassId')
            res_level_id = res_class.get('LevelId')
            res_class_index = res_class.get('ClassIndex')

        # 验证上级分类目录是否存在
        if class_pid:
            sql_pid_exists = 'SELECT Id FROM tb_template_class WHERE Id = %s AND Deleted = 0;'

            cur_task.execute(sql_pid_exists, [class_pid])
            res_class_pid = cur_task.fetchone()
            if not res_class_pid:
                res.update(code=ResponseCode.TempClassIdNotExists, msg=ResponseMessage.TempClassIdNotExists)
                return res.data
        else:
            class_pid = None

        # 验证模板分类层级是否大于4
        if class_pid:
            cur_task.execute(sql_level, [class_pid])
            res_class_count = cur_task.fetchall()
            if res_class_count:
                res_class_count = res_class_count[0].get('ClassCount')
                if res_class_count > 3:
                    res.update(code=ResponseCode.TempClassLevelMoreThan4, msg=ResponseMessage.TempClassLevelMoreThan4)
                    return res.data

        #  获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_class_update = 'UPDATE tb_template_class ' \
                           'SET UpdatedbyId = %s '
        if res_temp_id != temp_id:
            sql_class_update += ',TemplateId = %s '
            params_update.append(temp_id)
        if res_class_path != class_path:
            sql_class_update += ',TempClassPath = %s '
            params_update.append(class_path)
        if res_class_name != class_name:
            sql_class_update += ',TempClassName = %s '
            params_update.append(class_name)
        if res_class_pid != class_pid:
            sql_class_update += ',TempClassPId = %s '
            params_update.append(class_pid)
        if res_fclass_id != fclass_id:
            sql_class_update += ',FeatureClassId = %s '
            params_update.append(fclass_id)
        if res_level_id != level_id:
            sql_class_update += ',LevelId = %s '
            params_update.append(level_id)
        if res_class_index != class_index:
            sql_class_update += ',ClassIndex = %s '
            params_update.append(class_index)
        sql_class_update += 'WHERE Id = %s;'
        params_update.append(class_id)
        cur_task.execute(sql_class_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetTempClass', methods=["POST"])
@auth.login_required
def gettc():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_id = req_data['TemplateId']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text = 'WITH RECURSIVE cte AS ( SELECT Id, TemplateId, TempClassPath, TempClassName, ' \
               'TempClassPId, FeatureClassId, LevelId, ClassIndex, ClassRemark, CreatedById, CreatedAt, ' \
               'UpdatedbyId, UpdatedAt FROM tb_template_class WHERE Deleted = 0 '
    params = []
    if isinstance(temp_id, str):
        sql_text += 'AND Id=%s ' \
                    'UNION ALL SELECT t.Id, t.TemplateId, t.TempClassPath, t.TempClassName,t.TempClassPId, ' \
                    't.FeatureClassId, t.LevelId, t.ClassIndex, t.ClassRemark, t.CreatedById, t.CreatedAt, ' \
                    't.UpdatedbyId, t.UpdatedAt FROM tb_template_class t JOIN cte c ON t.TempClassPId = c.Id ' \
                    'WHERE t.Deleted = 0) SELECT Id, TemplateId, TempClassPath, TempClassName, ' \
                    'IF(Id=%s,NULL,TempClassPId) AS TempClassPId, FeatureClassId, LevelId, ClassIndex, ' \
                    'ClassRemark, CreatedById, CreatedAt, UpdatedbyId, UpdatedAt FROM cte ORDER BY Id;'
        params.append(temp_id)
        params.append(temp_id)
    elif temp_id is None:
        sql_text += 'AND TempClassPId IS NULL ' \
                    'UNION ALL SELECT t.Id, t.TemplateId, t.TempClassPath, t.TempClassName,t.TempClassPId, ' \
                    't.FeatureClassId, t.LevelId, t.ClassIndex, t.ClassRemark, t.CreatedById, t.CreatedAt, ' \
                    't.UpdatedbyId, t.UpdatedAt FROM tb_template_class t JOIN cte c ON t.TempClassPId = c.Id ' \
                    'WHERE t.Deleted = 0) SELECT Id, TemplateId, TempClassPath, TempClassName, ' \
                    'TempClassPId, FeatureClassId, LevelId, ClassIndex, ' \
                    'ClassRemark, CreatedById, CreatedAt, UpdatedbyId, UpdatedAt FROM cte ORDER BY Id;'
    else:
        result = dict(error='参数Id传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        new_tree = TempClassTree(data=result)
        result_tree = new_tree.build_tree()
        res.update(data=result_tree)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/AddTempDetail', methods=["POST"])
@auth.login_required
def addtd():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_name = req_data['TemplateName']
        temp_sta = req_data['TemplateStatus']

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证模板名称是否存在
        sql_name_exists = 'SELECT Id FROM tb_template_info ' \
                          'WHERE TemplateName = %s AND Deleted = 0;'

        cur_task.execute(sql_name_exists, [temp_name])
        res_temp_name = cur_task.fetchone()
        if res_temp_name:
            res.update(code=ResponseCode.TempNameExists, msg=ResponseMessage.TempNameExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        # nextval
        cur_task.execute('SELECT fn_nextval(%s) AS Id', ['TI'])
        res_cur = cur_task.fetchone()
        temp_id = res_cur.get('Id')

        sql_temp_ins = 'insert into tb_template_info (Id, TemplateName, ' \
                       'TemplateStatus, CreatedById, UpdatedbyId) ' \
                       'values (%s, %s, %s, %s, %s, %s, %s, %s);'
        params_class = [temp_id, temp_name, temp_sta, current_user_id, current_user_id]

        cur_task.execute(sql_temp_ins, params_class)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/DelTempDetail', methods=["POST"])
@auth.login_required
def deltd():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_id = req_data['TemplateId']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证模版是否存在
        sql_id_exists = 'SELECT Id \
        FROM tb_template_info \
        WHERE Id = %s \
        AND Deleted = 0;'

        cur_task.execute(sql_id_exists, [temp_id])
        res_temp_id = cur_task.fetchall()
        if not res_temp_id:
            res.update(code=ResponseCode.TempIdNotExists, msg=ResponseMessage.TempIdNotExists)
            return res.data

        # 获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        sql_temp_del = 'UPDATE tb_template_info ' \
                       'SET DeletedById = %s, DeletedAt = NOW(), Deleted = 1 ' \
                       'WHERE Id = %s;'

        cur_task.execute(sql_temp_del, [current_user_id, temp_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/UpdateTempDetail', methods=["POST"])
@auth.login_required
def updatetd():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_id = req_data['TemplateId']
        temp_name = req_data['TemplateName']
        temp_sta = req_data['TemplateStatus']

    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)

        # 验证模板Id是否存在
        sql_id_exists = 'SELECT Id, TemplateName, TemplateStatus \
        FROM tb_template_info \
        WHERE Id = %s \
        AND Deleted = 0;'

        cur_task.execute(sql_id_exists, [temp_id])
        res_temp = cur_task.fetchone()
        if not res_temp:
            res.update(code=ResponseCode.TempIdNotExists, msg=ResponseMessage.TempIdNotExists)
            return res.data
        else:
            res_temp_name = res_temp.get('TemplateName')
            res_temp_sta = res_temp.get('TemplateStatus')

        # 验证模板名称是否存在
        sql_name_exists = 'SELECT 1 \
            FROM tb_template_info \
            WHERE TemplateName = %s \
            AND Deleted = 0;'

        cur_task.execute(sql_name_exists, [temp_name])
        res_temp = cur_task.fetchone()
        if res_temp:
            res.update(code=ResponseCode.TempNameExists, msg=ResponseMessage.TempNameExists)
            return res.data

        #  获取当前用户Id
        current_user = auth.current_user()
        sql_user_id = 'SELECT Id FROM tb_basic_user WHERE UserName = %s AND UserStatus = 1 AND Deleted = 0;'
        cur_task.execute(sql_user_id, [current_user])
        res_cur = cur_task.fetchone()
        if res_cur:
            current_user_id = res_cur.get('Id')
        else:
            current_user_id = ''

        params_update = [current_user_id]
        sql_temp_update = 'UPDATE tb_template_info ' \
                          'SET UpdatedbyId = %s '
        if res_temp_name != temp_name:
            sql_temp_update += ',TemplateName = %s '
            params_update.append(temp_name)
        if res_temp_sta != temp_sta:
            sql_temp_update += ',TemplateStatus = %s '
            params_update.append(temp_sta)
        sql_temp_update += 'WHERE Id = %s;'
        params_update.append(temp_id)
        cur_task.execute(sql_temp_update, params_update)
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data


@route(bp, '/GetTempDetail', methods=["POST"])
@auth.login_required
def gettd():
    res = ResMsg()
    try:
        req_data = request.get_json(force=True)
        temp_id = req_data['Id']
        temp_name = req_data['TemplateName']
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    sql_text = 'SELECT * FROM tb_template_info a WHERE 1=1 '
    params = []
    if isinstance(temp_id, str):
        sql_text += "AND Id = %s "
        params.append(temp_id)
    elif temp_id is None:
        pass
    else:
        result = dict(error='参数Id传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    if isinstance(temp_name, str):
        sql_text += "AND TemplateName = %s "
        params.append(temp_name)
    elif temp_name is None:
        pass
    else:
        result = dict(error='参数Id传输错误')
        res.update(code=ResponseCode.InvalidParameter, msg=ResponseMessage.InvalidParameter, data=result)
        return res.data
    try:
        con_task = current_app.config['po_task'].get_connection()
        cur_task = con_task.cursor(dictionary=True)
        cur_task.execute(sql_text, params)
        result = cur_task.fetchall()
        res.update(data=result)
    except Exception as e:
        result = dict(error=str(e))
        res.update(code=ResponseCode.SqlError, msg=ResponseMessage.SqlError, data=result)
    finally:
        cur_task.close()
        con_task.close()
    return res.data
