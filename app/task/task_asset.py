#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/7/6 09:18

import yaml
import os
from mysql.connector.pooling import MySQLConnectionPool

pwd = os.getcwd()
config_path = os.path.join(pwd, 'config/config.yaml')
with open(config_path, 'r', encoding='utf-8') as f:
    conf = yaml.safe_load(f.read())

task_cfg_db = conf['PRODUCTION']['DATABASES']
task_cfg_task = task_cfg_db.get('DB_TASK')
task_po_task = MySQLConnectionPool(host=task_cfg_task['host'],
                                   user=task_cfg_task['user'],
                                   password=task_cfg_task['password'],
                                   db=task_cfg_task['database'],
                                   port=task_cfg_task['port'],
                                   charset='utf8mb4',
                                   pool_name='po_task',
                                   pool_size=5,
                                   time_zone='+8:00')


def collect_instance(job_id):
    try:
        con_task = task_po_task.get_connection()
        cur_task = con_task.cursor(dictionary=True)

        sql_text = 'insert into task_test(JobId) values(%s);'
        cur_task.execute(sql_text, [job_id])
        con_task.commit()
    except Exception as e:
        con_task.rollback()
        print(e)
    finally:
        cur_task.close()
        con_task.close()
    return True


def collect_sample():
    pass
